Izg_ignite_PMC234:
	最原始的代码，芯片型号为PMC234,未做更改。
	the orginal code source, the chip model is PMC234, has not been changed.

Izg_ignite_PFS122:
	手枪式遥控型爬墙车，芯片型号变更为PFS122,软件控制逻辑与PMC234一样。
	Pistol remote control wall-climbing vehicle, the chip model is changed to PFS122, and the software control logic is the same as PMC234.

lzg_44649_PFS122:
	按键式遥控型爬墙车，芯片型号为PFS122
	Button remote control wall-climbing vehicle, the chip model is PFS122.
	
remote_tx:
	按键式遥控器，芯片型号为PFS122
	Button remote control, the chip model is PFS122.

ZERO-G WallCar:
	主控芯片为PMS232。逻辑控制与PMC234版一样