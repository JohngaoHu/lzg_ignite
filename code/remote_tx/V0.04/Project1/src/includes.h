#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#include "../src/typedef.h"
#include "../src/extern.h"
#include "../src/variables.h"

#include "../src/board.h"
#include "../src/dev_key.h"
#include "../src/dev_battery.h"

#include "../src/hal_pwm.h"
#include "../src/hal_sleep.h"
#include "../src/hal_adc.h"

#endif

