#include "../src/includes.h"

/*******************************************************************************************************
 * functional	:	git gpio vol level
 * argument in 	:	(temporary.vars_16b[0]) key
 * argument out :	(temporary.vars_16b[1])
 * description	:	none
 * ****************************************************************************************************/
void Button_Read_lever(void)
{
    switch (temporary.vars_16b[0])
    {
    case BtnName_trimleft:
        if (PINS_NAME_TRIMLEFT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_trimright:
        if (PINS_NAME_TRIMRIGHT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_forwardkey:
        if (PINS_NAME_FORWARDKEY == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_backwardkey:
        if (PINS_NAME_BACKWARDKEY == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_steeringleft:
        if (PINS_NAME_STEERINGLEFT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_steeringright:
        if (PINS_NAME_STEERINGRIGHT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_idchoose:
        if (PINS_NAME_IDCHOOSE == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;
    }
}

/*******************************************************************************************************
 * functional	:	read key
 * argument in 	:	(temporary.vars_8b[0]) step
 *                  (temporary.vars_8b[1]) shake
 *                  (temporary.vars_8b[2]) key
 * argument out :	(temporary.vars_8b[3]) btn_press or btn_null
 *                  (temporary.vars_8b[0]) step
 *                  (temporary.vars_8b[1]) shake
 * description	:	none
 * ****************************************************************************************************/
void Button_Read_Single(void)
{
    u8 valid = 0;

    temporary.vars_8b[3] = btn_null;

    switch (temporary.vars_8b[0])
    {
    case 0:                                           // detect
        temporary.vars_16b[0] = temporary.vars_8b[2]; // get key number
        Button_Read_lever();                          // read gpio level
        if (temporary.vars_16b[1] == 0)               // if level = 0, valid
        {                                             //
            temporary.vars_8b[1] = 0;                 // clear shake
            temporary.vars_8b[0]++;                   // step increase
        }
        break;

    case 1:                                           // shake
        temporary.vars_16b[0] = temporary.vars_8b[2]; // get key number
        Button_Read_lever();                          // read gpio level
        if (temporary.vars_16b[1] == 0)               // if level = 0, valid
        {                                             //
            temporary.vars_8b[1]++;                   // shake++
            if (temporary.vars_8b[1] > KEY_SHAKE_MAX) // pass
            {                                         //
                temporary.vars_8b[0]++;               // step increase
                temporary.vars_8b[1] = 0;             // clear shake
                temporary.vars_8b[3] = btn_press;     // return valid
            }                                         //
        }                                             //
        else                                          //
        {                                             // incorrect
            temporary.vars_8b[0] = 0;                 // step clear
        }
        break;

    case 2:                                             // wait key release
        temporary.vars_16b[0] = temporary.vars_8b[2];   // get key number
        Button_Read_lever();                            // read gpio level
        if (temporary.vars_16b[1] == 0)                 // if level = 0, valid
        {                                               //
            temporary.vars_8b[1] = 0;                   // clear shake
            temporary.vars_8b[3] = btn_press;           // return valid
        }                                               //
        else                                            //
        {                                               //
            temporary.vars_8b[1]++;                     // shake++
            if (temporary.vars_8b[1] > KEY_RELEASE_MIN) // reach long press threshold
            {                                           //
                temporary.vars_8b[3] = btn_null;        //
                temporary.vars_8b[1] = 0;               // clear shake
                temporary.vars_8b[0] = 0;               // clear step
            }
        }
        break;
    }
}

/*******************************************************************************************************
 * functional	:	user code enter
 * argument in 	:	none
 * argument out :	none
 * description	:	none
 * ****************************************************************************************************/
void Key_Scan_Function(void)
{
    if (dev_key.period > 0)
    {
        return;
    }
    dev_key.period = SYSTICE_MS * 5;

    //**************trimleft****************************
    temporary.vars_8b[0] = dev_key.trimleft.step;
    temporary.vars_8b[1] = dev_key.trimleft.shake;
    temporary.vars_8b[2] = BtnName_trimleft;
    Button_Read_Single();
    dev_key.trimleft.step = temporary.vars_8b[0];
    dev_key.trimleft.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_TRIMLEFT;
    else
        dev_key.state &= ~KEYBOARD_BITS_TRIMLEFT;

    //**************trimright****************************
    temporary.vars_8b[0] = dev_key.trimright.step;
    temporary.vars_8b[1] = dev_key.trimright.shake;
    temporary.vars_8b[2] = BtnName_trimright;
    Button_Read_Single();
    dev_key.trimright.step = temporary.vars_8b[0];
    dev_key.trimright.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_TRIMRIGHT;
    else
        dev_key.state &= ~KEYBOARD_BITS_TRIMRIGHT;

    //**************forwardkey****************************
    temporary.vars_8b[0] = dev_key.forwardkey.step;
    temporary.vars_8b[1] = dev_key.forwardkey.shake;
    temporary.vars_8b[2] = BtnName_forwardkey;
    Button_Read_Single();
    dev_key.forwardkey.step = temporary.vars_8b[0];
    dev_key.forwardkey.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_FORWARDKEY;
    else
        dev_key.state &= ~KEYBOARD_BITS_FORWARDKEY;

    //**************backwardkey****************************
    temporary.vars_8b[0] = dev_key.backwardkey.step;
    temporary.vars_8b[1] = dev_key.backwardkey.shake;
    temporary.vars_8b[2] = BtnName_backwardkey;
    Button_Read_Single();
    dev_key.backwardkey.step = temporary.vars_8b[0];
    dev_key.backwardkey.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_BACKWARDKEY;
    else
        dev_key.state &= ~KEYBOARD_BITS_BACKWARDKEY;

    //**************steeringleft****************************
    temporary.vars_8b[0] = dev_key.steeringleft.step;
    temporary.vars_8b[1] = dev_key.steeringleft.shake;
    temporary.vars_8b[2] = BtnName_steeringleft;
    Button_Read_Single();
    dev_key.steeringleft.step = temporary.vars_8b[0];
    dev_key.steeringleft.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_STEERINGLEFT;
    else
        dev_key.state &= ~KEYBOARD_BITS_STEERINGLEFT;

    //**************steeringright****************************
    temporary.vars_8b[0] = dev_key.steeringright.step;
    temporary.vars_8b[1] = dev_key.steeringright.shake;
    temporary.vars_8b[2] = BtnName_steeringright;
    Button_Read_Single();
    dev_key.steeringright.step = temporary.vars_8b[0];
    dev_key.steeringright.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_STEERINGRIGHT;
    else
        dev_key.state &= ~KEYBOARD_BITS_STEERINGRIGHT;

    //**************idchoose****************************
    temporary.vars_8b[0] = dev_key.idchoose.step;
    temporary.vars_8b[1] = dev_key.idchoose.shake;
    temporary.vars_8b[2] = BtnName_idchoose;
    Button_Read_Single();
    dev_key.idchoose.step = temporary.vars_8b[0];
    dev_key.idchoose.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
    {
        dev_key.state |= KEYBOARD_BITS_IDCHOOSE;
        ir_id_slot = IR_ID1_SLOT;
    }
    else
    {
        dev_key.state &= ~KEYBOARD_BITS_IDCHOOSE;
        ir_id_slot = IR_ID0_SLOT;
    }

    //***************gain  key number*************************
    temporary.vars_8b[2] = dev_key.state & 0xfc;
    if (temporary.vars_8b[2] == KEYBOARD_BITS_FORWARDKEY) // only forward
    {
        temporary.vars_8b[0] = Key_Result_Up;
    }
    else if (temporary.vars_8b[2] == KEYBOARD_BITS_BACKWARDKEY) // only backward
    {
        temporary.vars_8b[0] = Key_Result_Dn;
    }
    else if (temporary.vars_8b[2] == KEYBOARD_BITS_STEERINGLEFT) // only left sharp
    {
        temporary.vars_8b[0] = Key_Result_Left;
    }
    else if (temporary.vars_8b[2] == KEYBOARD_BITS_STEERINGRIGHT) // only right sharp
    {
        temporary.vars_8b[0] = Key_Result_Right;
    }
    else if (temporary.vars_8b[2] == KEYBOARD_BITS_TRIMLEFT) // only trim left
    {
        temporary.vars_8b[0] = Key_Result_TrimLe;
    }
    else if (temporary.vars_8b[2] == KEYBOARD_BITS_TRIMRIGHT) // only trim right
    {
        temporary.vars_8b[0] = Key_Result_TrimRi;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_TRIMLEFT)) // forward + trim left
    {
        temporary.vars_8b[0] = Key_Result_UpTrimL;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_TRIMRIGHT)) // forward + trim right
    {
        temporary.vars_8b[0] = Key_Result_UpTrimR;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_STEERINGLEFT)) // forward + left sharp
    {
        temporary.vars_8b[0] = Key_Result_UpLeft;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_STEERINGRIGHT)) // forward + right sharp
    {
        temporary.vars_8b[0] = Key_Result_UpRight;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_TRIMLEFT)) // backward + trim left
    {
        temporary.vars_8b[0] = Key_Result_DnTrimL;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_TRIMRIGHT)) // backward + trim right
    {
        temporary.vars_8b[0] = Key_Result_DnTrimR;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGLEFT)) // backward + left sharp
    {
        temporary.vars_8b[0] = Key_Result_DnLeft;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGRIGHT)) // backward + right sharp
    {
        temporary.vars_8b[0] = Key_Result_DnRight;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGLEFT)) //forward+backward+left
    {
        temporary.vars_8b[0] = Key_Result_Left;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGRIGHT)) //forward+backward+left
    {
        temporary.vars_8b[0] = Key_Result_Right;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_STEERINGLEFT | KEYBOARD_BITS_STEERINGRIGHT)) //forward+left+right
    {
        temporary.vars_8b[0] = Key_Result_Up;
    }
    else if (temporary.vars_8b[2] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGLEFT | KEYBOARD_BITS_STEERINGRIGHT)) //backward+left+right
    {
        temporary.vars_8b[0] = Key_Result_Dn;
    }
    else // stop
    {
        temporary.vars_8b[0] = Key_Result_Null;
    }

    temporary.vars_8b[0] <<= 1;
    if (dev_key.state & KEYBOARD_BITS_IDCHOOSE)
    {
        temporary.vars_8b[0] |= 0x01;
    }

    dev_wave38k.data = 0;
    dev_wave38k.data |= temporary.vars_8b[0];
    dev_wave38k.data <<= 5;
    dev_wave38k.data |= (~temporary.vars_8b[0]) & 0X1f;

    //******************detect data send enable*****************
    if (dev_key.state & 0xfc) // any keys had pressed
    {                         //
        dev_sleep.tick = 0;   // clear tick amount
    }

    if (dev_wave38k.step == 0xff) //need to send data whether there is a key pressed or not, until dev_sleep.tick timeout;
    {
        switch (dev_key.step)
        {
        case 0:
            if (ir_slot_index == IR_START_SLOT)
            {
                ir_data_buffer = dev_wave38k.data; // irq might happen during key scan , shall not use data , only use final result
                dev_wave38k.step = 0;              // means start transmit
                dev_key.step++;                    //
            }
            break;

        default:
            if (ir_slot_index == ir_id_slot)
            {
                ir_data_buffer = dev_wave38k.data; // irq might happen during key scan , shall not use data , only use final result
                dev_wave38k.step = 0;              // means start transmit
                dev_key.step = 0;                  //
            }
            break;
        }
    }
}
