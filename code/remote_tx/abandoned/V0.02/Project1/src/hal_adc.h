#ifndef _HAL_ADC_H_
#define _HAL_ADC_H_

struct ADC_CTR
{
    u8 step;

    /*struct ADC_PB7_TYPE
    {
        u16 sum;
        u16 average;
    } adc_pb7;
    */
   struct ADC_Bandgap_TYPE
    {
        u16 sum;
        u16 average;
    } adc_bandgap;
};

void ADC_Process_Modul(void);
#endif
