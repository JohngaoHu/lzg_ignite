#ifndef _HAL_SLEEP_H_
#define _HAL_SLEEP_H_


#define SLEEP_TICK_MAX  0xffff
#define SLEEP_ENTER_OVER 5000 //ms

struct Sleep_ctr{
    u16 ms;
    u16 tick;
};

void Sleep_Read_Statu(void);
#endif

