#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

typedef bit bool;
typedef byte uint8_t;
typedef word uint16_t;

typedef byte u8;
typedef word u16;
typedef eword u24;
typedef dword u32;

#define BIT0 (1 << 0)
#define BIT1 (1 << 1)
#define BIT2 (1 << 2)
#define BIT3 (1 << 3)

#define BIT4 (1 << 4)
#define BIT5 (1 << 5)
#define BIT6 (1 << 6)
#define BIT7 (1 << 7)

#define BIT8 (1 << 8)
#define BIT9 (1 << 9)
#define BIT10 (1 << 10)
#define BIT11 (1 << 11)

#define BIT12 (1 << 12)
#define BIT13 (1 << 13)
#define BIT14 (1 << 14)
#define BIT15 (1 << 15)

#define BIT16 (1 << 16)
#define BIT17 (1 << 17)
#define BIT18 (1 << 18)
#define BIT19 (1 << 19)

#define BIT20 (1 << 20)
#define BIT21 (1 << 21)
#define BIT22 (1 << 22)
#define BIT23 (1 << 23)

#define BIT24 (1 << 24)
#define BIT25 (1 << 25)
#define BIT26 (1 << 26)
#define BIT27 (1 << 27)

#define BIT28 (1 << 28)
#define BIT29 (1 << 29)
#define BIT30 (1 << 30)
#define BIT31 (1 << 31)

bit	GPIO_Pin_trimleft:      PA.7
bit	GPIO_Pin_trimright:     PA.6
bit	GPIO_Pin_idchoose:      PA.0
bit	GPIO_Pin_forwardkey:    PB.5
bit	GPIO_Pin_steeringright: PB.3
bit	GPIO_Pin_steeringleft:  PB.1
bit	GPIO_Pin_backwardkey:   PB.0

bit GPIO_Pin_BatCkeEn:      PB.4

#define GLOBAL_INTERRUPT_ENABLE engint
#define GLOBAL_INTERRUPT_DISABLE disgint

#define SYSTICE_MS   20
#define SECRET_KEY   0x15
#define OPTIONAL_SLEEP_EN (1)

#endif

