#include "../src/includes.h"

static u8 step = 0;
/*******************************************************************************************************
 * functional	:	Init
 * argument in 	:	none
 * argument out :	none
 * description	:	none
 * ****************************************************************************************************/
void Pwm_Init_Tim2(void)
{
    tm2ct = 0x00;          // counter register
    tm2b = TM2B_PWM_38K;   // bound register
    tm2s = 0b_0_00_00001;  // 8bit_/1_/9
    tm2c = 0b_0001_10_0_0; // clk_PA3_period_en
}

/*******************************************************************************************************
 * functional	:	callback function
 * argument in 	:	none
 * argument out :	none
 * description	:	generate data packet
 * ****************************************************************************************************/
void Wave_Generate_irq(void)
{
    switch (dev_wave38k.step)
    {
    case 0: // head first
        PMW38K_WAVE_EN;
        dev_wave38k.period = 0;
        dev_wave38k.step++;
        break;

    case 1: // wait time out
        dev_wave38k.period++;
        if (dev_wave38k.period > WAVE_HEAD_FIRST)
        {
            dev_wave38k.period = 0;
            dev_wave38k.step++;
        }
        break;

    case 2: // head next
        PMW38K_WAVE_DIS;
        dev_wave38k.period = 0;
        dev_wave38k.step++;
        break;

    case 3: // wait time out
        dev_wave38k.period++;
        if (dev_wave38k.period > WAVE_HEAD_NEXT)
        {
            dev_wave38k.period = 0;
            dev_wave38k.index = 0x01;
            dev_wave38k.step++;
        }
        break;

    case 4: // data-head
        PMW38K_WAVE_EN;
        dev_wave38k.period = 0;
        dev_wave38k.step++;
        break;

    case 5: // wait time out
        dev_wave38k.period++;
        if (dev_wave38k.period > WAVE_BIT_HEAD)
        {
            dev_wave38k.period = 0;
            dev_wave38k.step++;
        }
        break;

    case 6: // data bit
        PMW38K_WAVE_DIS;
        dev_wave38k.period = 0;
        dev_wave38k.step++;
        break;

    case 7:                                         //
        dev_wave38k.period++;                       //
        if (dev_wave38k.data & dev_wave38k.index)   // bit 1
        {                                           //
            if (dev_wave38k.period > WAVE_BIT_BIT1) //
            {                                       //
                dev_wave38k.period = 0;             //
                dev_wave38k.index <<= 1;            //
                if (dev_wave38k.index == 0)         //
                {                                   //
                    dev_wave38k.step = 0xfd;        //
                }                                   //
                else                                //
                {                                   //
                    dev_wave38k.step = 4;           //
                }                                   //
            }                                       //
        }                                           //
        else                                        // bit 0
        {                                           //
            if (dev_wave38k.period > WAVE_BIT_BIT0) //
            {                                       //
                dev_wave38k.period = 0;             //
                dev_wave38k.index <<= 1;            //
                if (dev_wave38k.index == 0)         //
                {                                   //
                    dev_wave38k.step = 0xfd;        //
                }                                   //
                else                                //
                {                                   //
                    dev_wave38k.step = 4;           //
                }                                   //
            }                                       //
        }                                           //
        break;

    case 0xfd:
        PMW38K_WAVE_EN;
        dev_wave38k.period = 0;
        dev_wave38k.step++;
        break;

    case 0xfe:
        dev_wave38k.period++;
        if (dev_wave38k.period > WAVE_BIT_HEAD)
        {
            dev_wave38k.period = 0;
            switch (step)
            {
            case 0:
                if (dev_key.state & KEYBOARD_BITS_IDCHOOSE)
                {
                    dev_wave38k.delay = 40 * SYSTICE_MS;
                }
                else
                {
                    dev_wave38k.delay = 40 * SYSTICE_MS;
                }
                step++;
                break;

            case 1:
                dev_wave38k.delay = 40 * SYSTICE_MS;
                step++;
                break;

            default:
                if (dev_key.state & KEYBOARD_BITS_IDCHOOSE)
                {
                    dev_wave38k.delay = 40 * SYSTICE_MS;
                }
                else
                {
                    dev_wave38k.delay = 40 * SYSTICE_MS;
                }
                step = 0;
                break;
            }
            dev_wave38k.step++;
        }
        break;

    case 0xff: // space more than 40ms
        PMW38K_WAVE_DIS;
        dev_wave38k.period++;
        if (dev_wave38k.period > dev_wave38k.delay)
        {
            dev_wave38k.period = 0;
            dev_wave38k.step = 0;
        }
        break;
    }
}
