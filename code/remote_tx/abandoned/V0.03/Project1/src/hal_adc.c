#include "includes.h"

#define ADC_ADCC_PB7 0b1_0_0111_00     //enable adc, not start, PB7
#define ADC_ADCC_BANDGAP 0b1_0_1111_00 //enable adc, not start, bandgap

#define Bat_Measure_Enable() GPIO_Pin_BatCkeEn = 0
#define Bat_Measure_Disable() GPIO_Pin_BatCkeEn = 1
/*******************************************************************************************************
 * functional	:	event
 * argument in 	:	none
 * argument out :	none
 * description	:	12bit  range: 0~4096
 * ****************************************************************************************************/
void ADC_Process_Modul(void)
{
    switch (dev_adc.step)
    {
    case 0:
        //Bat_Measure_Enable();
        adcc = ADC_ADCC_BANDGAP;
        AD_Start = 1;
        dev_adc.step++;
        break;

    case 1:
        if (AD_DONE)
        {
            WORD Data;
            Data$1 = ADCRH;
            Data$0 = ADCRL;
            Data = Data >> 4;
            dev_adc.adc_bandgap.sum += Data;
            dev_adc.adc_bandgap.average = dev_adc.adc_bandgap.sum >> 5;
            dev_adc.adc_bandgap.sum -= dev_adc.adc_bandgap.average;
            dev_adc.step = 0;
        }
        break;

    default:
        dev_adc.step = 0;
        break;
    }
}
