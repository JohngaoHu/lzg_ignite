#include "../src/includes.h"

/*******************************************************************************************************
 * functional	:	user code enter
 * argument in 	:	none
 * argument out :	none
 * description	:	none
 * ****************************************************************************************************/
void Battery_Task_Modul(void)
{
    if (dev_battery.period > 0)
    {
        return;
    }
    dev_battery.period = SYSTICE_MS * 10;

}
