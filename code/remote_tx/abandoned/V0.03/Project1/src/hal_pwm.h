#ifndef _DEV_PWM_H_
#define _DEV_PWM_H_

#define TM2B_PWM_38K 25

#define WAVE_HEAD_FIRST 90 //4.5ms 90*50 = 4500us
#define WAVE_HEAD_NEXT 46  //2.3ms 46*50 = 2300us
#define WAVE_BIT_HEAD 8    //560us 8*50 = 400us , need to check , Justus
#define WAVE_BIT_BIT0 8    //560us 8*50 = 400us , need to check , Justus
#define WAVE_BIT_BIT1 24   //1.68ms 24*50 = 1200us , need to check , Justus


// packet time , need to check , Justus
// head1+head2+5*bit1+5*bit0+11*bithead  , also need to add 50us stage change steps 

#define IR_SLOT_TIME_MS (25)

#define IR_START_SLOT (1)
#define IR_ID0_SLOT (3)
#define IR_ID1_SLOT (5)
#define IR_END_SLOT (10)



#define PMW38K_WAVE_EN         \
    {                          \
        tm2ct = 0x00;          \
        tm2b = TM2B_PWM_38K;   \
        tm2s = 0b_0_00_00001;  \
        tm2c = 0b_0001_10_0_1; \
    }

#define PMW38K_WAVE_DIS \
    {                   \
        tm2ct = 0x00;   \
        tm2b = 0;       \
        tm2s = 0;       \
        tm2c = 0;       \
    }

#define PWM_WAVESEND_ENABLE (1 << 7)

struct Wave38k_Ctr
{
    u8 statu;
    u8 step;
    u16 period;
    u16 delay;
    u16 index;
    u16 data;
};

void Pwm_Init_Tim2(void);
void Wave_Generate_irq(void);
#endif
