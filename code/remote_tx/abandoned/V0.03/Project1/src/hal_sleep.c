#include "../src/includes.h"

/*************************************************************************
 * function     :       
 * argument in  :     
 * argument out :  
 * description  :   
 * ***********************************************************************/
void Sleep_Read_Statu(void)
{
    PMW38K_WAVE_DIS;
/*
    Note:
        if it is already in sleep mode,there are two ways as below to wake it up.
        1. Plug in the usb charging cable
        2. push the switch into wall climing mode
*/
#if OPTIONAL_SLEEP_EN
    disgint;      //disable global interrupt
    inten = 0x00; //operate coressponding channal interrupt
    eoscr = 0x01; //off external oscillator && bandgap power down
    adcc = 0x00;  //off adc covertor

    /*
    PA7 : trimleft-key      input   pull up     wakeup
    PA6 : trimright-key     input   pull up     wakeup
    PA5 : float             output  low
    PA4 : float             output  low
    PA3 : 38K waveform out  output  low
    PA2 : no connect        output  low
    PA1 : no connect        output  low
    PA0 : id-choose         output  low
    */
    padier = 0xc0; //PA7 PA6 wakeup
    pa = 0x00;     // port
    pac = 0x3f;    // direction 0 = in
    paph = 0xc0;   // pa7 pull up, pa6 pull up

    /*
    PB7 : no connect        output  low
    PB6 : no connect        output  low
    PB5 : forward           input   pull up     wakeup
    PB4 : no connect        output  low
    PB3 : steering left     input   pull up     wakeup
    PB2 : no connect        output  low
    PB1 : steering right    input   pull up     wakeup
    PB0 : backward          input   pull up     wakeup
    */
    pbdier = 00101011b; //no wake up
    pb = 0x00;          // port
    pbc = 11010100b;    // direction (0)-in (1)-out
    pbph = 00101011b;   // pull up resistor (1) Enable

    stopsys;
#endif
}
