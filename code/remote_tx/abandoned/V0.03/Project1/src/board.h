#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#define   gotosleep_timover_size     300      //unit: second

void Board_Initialize_Global(void);
void Callback_Sofetim_20ms(void);
void Sleep_Read_Statu(void);
void Mem_Read_u8_array(void);
void Mem_Read_u16_array(void);
#endif
