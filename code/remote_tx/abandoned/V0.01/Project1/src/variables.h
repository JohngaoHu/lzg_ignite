#ifndef _VARIABLES_H_
#define _VARIABLES_H_

#include "../src/board.h"
#include "../src/dev_key.h"
#include "../src/dev_battery.h"

#include "../src/hal_pwm.h"
#include "../src/hal_sleep.h"
#include "../src/hal_adc.h"

/*
    Note:
        This set of variables is not allowed to be called during interrupts
        its greatest use is to simulate calls to functions with arguments
*/
struct temporary_type //???±±???
{
    u8 vars_8b[4];
    u16 vars_16b[4];
};

struct system_ctr{
    u8 step;
};


.ramadr 0x00 
uint16_t MemAddr_Base; //在作内存清空的时候，指向内存的指针变量，必须是一个word变量，同时其指针小于0xfe
struct temporary_type temporary;  //中间变量，切不可在中断中引用
struct Key_CTR dev_key;
struct Battery_CTR dev_battery;
struct Wave38k_Ctr dev_wave38k;
struct Sleep_ctr dev_sleep;
struct system_ctr dev_system;
struct ADC_CTR dev_adc;
#endif

