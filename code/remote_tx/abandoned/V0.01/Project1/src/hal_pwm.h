#ifndef _DEV_PWM_H_
#define _DEV_PWM_H_

#define TM2B_PWM_38K 25

#define WAVE_HEAD_FIRST 90 //4.5ms
#define WAVE_HEAD_NEXT 46  //2.3ms
#define WAVE_BIT_HEAD 11   //560us
#define WAVE_BIT_BIT0 11   //560us
#define WAVE_BIT_BIT1 33   //1.68ms

#define PMW38K_WAVE_EN         \
    {                          \
        tm2ct = 0x00;          \
        tm2b = TM2B_PWM_38K;   \
        tm2s = 0b_0_00_00001;  \
        tm2c = 0b_0001_10_0_1; \
    }

#define PMW38K_WAVE_DIS \
    {                   \
        tm2ct = 0x00;   \
        tm2b = 0;       \
        tm2s = 0;       \
        tm2c = 0;       \
    }


#define PWM_WAVESEND_ENABLE  (1 << 7)

struct Wave38k_Ctr
{
    u8 statu;
    u8 step;
    u16 period;

    u16 data;
    u16 index;
};

void Pwm_Init_Tim2(void);
void Wave_Generate_irq(void);
#endif
