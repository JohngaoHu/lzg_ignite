#ifndef _DEV_KEY_H_
#define _DEV_KEY_H_
#include "../src/typedef.h"

#define KEY_SHAKE_MAX  2
#define KEY_LONG_SHORT 160
#define KEY_RELEASE_MIN 20
#define PINS_NAME_TRIMLEFT GPIO_Pin_trimleft
#define PINS_NAME_TRIMRIGHT GPIO_Pin_trimright
#define PINS_NAME_FORWARDKEY GPIO_Pin_forwardkey
#define PINS_NAME_BACKWARDKEY GPIO_Pin_backwardkey
#define PINS_NAME_STEERINGLEFT GPIO_Pin_steeringleft
#define PINS_NAME_STEERINGRIGHT GPIO_Pin_steeringright
#define PINS_NAME_IDCHOOSE GPIO_Pin_idchoose

enum
{
    btn_null = 0,
    btn_press,
    btn_continuous,
};

enum
{
    BtnName_trimleft = 0,
    BtnName_trimright,
    BtnName_forwardkey,
    BtnName_backwardkey,
    BtnName_steeringleft,
    BtnName_steeringright,
    BtnName_idchoose,
};

#define KEYBOARD_BITS_TRIMLEFT (1 << 7)
#define KEYBOARD_BITS_TRIMRIGHT (1 << 6)
#define KEYBOARD_BITS_FORWARDKEY (1 << 5)
#define KEYBOARD_BITS_BACKWARDKEY (1 << 4)
#define KEYBOARD_BITS_STEERINGLEFT (1 << 3)
#define KEYBOARD_BITS_STEERINGRIGHT (1 << 2)
#define KEYBOARD_BITS_IDCHOOSE (1 << 1)

struct Key_CTR
{
    u16 period;
    u8 state;

    struct Trimleft_CTR{
        u8 step;
        u8 shake;
    }trimleft;

    struct Trimright_CTR{
        u8 step;
        u8 shake;
    }trimright;

    struct Forwardkey_CTR{
        u8 step;
        u8 shake;
    }forwardkey;

    struct Backwardkey_CTR{
        u8 step;
        u8 shake;
    }backwardkey;

    struct Steeringleft_CTR{
        u8 step;
        u8 shake;
    }steeringleft;

    struct Steeringright_CTR{
        u8 step;
        u8 shake;
    }steeringright;

    struct Idchoose_CTR{
        u8 step;
        u8 shake;
    }idchoose;
};

extern struct Key_CTR dev_key;
void Key_Scan_Function(void);
#endif
