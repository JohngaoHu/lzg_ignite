#include "../src/includes.h"

/***************************************************************************
 * function:        initialize t16
 * argument in:     none
 * argument out:    none
 * description:     1ms
 * *************************************************************************/
void T16_Initialize_Irq(void)
{
    $ t16m SYSCLK, / 4, BIT8; // 2us/step @2M, 1us/step @4M  BIT8含义是比较数值为8位，最大255溢出
}

/***************************************************************************
 * function:        initialize all io
 * argument in:     none
 * argument out:    none
 * description:     1 OUT  0 IN
 * *************************************************************************/
void Global_Initialize_GPIO(void)
{
    /*
    PA7 : trimleft-key      input   pull up     wakeup
    PA6 : trimright-key     input   pull up     wakeup
    PA5 : float             output  low
    PA4 : float             output  low
    PA3 : 38K waveform out  output  low
    PA2 : no connect        output  low
    PA1 : no connect        output  low
    PA0 : id-choose         input   pull up
    */
    padier = 1100_0001b; // 1 = dg-in & wakeup, if pin config analog function, dier must set to 0
    pa = 0000_0000b;     // port
    pac = 0011_1110b;    // direction (0)-in (1)-out
    paph = 1100_0001b;   // pull up resistor (1) Enable

     /*
    PB7 : no connect        output  low
    PB6 : no connect        output  low
    PB5 : forward           input   pull up     wakeup
    PB4 : no connect        output  low
    PB3 : steering left     input   pull up     wakeup
    PB2 : no connect        output  low
    PB1 : steering right    input   pull up     wakeup
    PB0 : backward          input   pull up     wakeup
    */
    pbdier = 00101011b; // 1 = dg-in & wakeup, if pin config analog function, dier must set to 0
    pb = 01000000b;     // port
    pbc =  11010100b;    // direction (0)-in (1)-out
    pbph = 00101011b;   // pull up resistor (1) Enable
}

/***************************************************************************
 * function:        initialize peripheral equipment
 * argument in:     none
 * argument out:    none
 * description:
 * *************************************************************************/
void Board_Initialize_Global(void)
{
    clkmd = 00010000b;        //IHRC/4,wdt dis, PA5 as IO
    intrq = 0x00;             //
    eoscr = 0x00;             //bandgap & lvr: normal
    Global_Initialize_GPIO(); //configure gpio
    T16_Initialize_Irq();     //configure tim 16
    inten |= (1 << 2);        //enable tm16
    //inten |= (1 << 3);        //enable adc irq
    intrq = 0x00;             //clear all interrupt flag
    GLOBAL_INTERRUPT_ENABLE;  //enable global interrupt sw

    Pwm_Init_Tim2();
}



