#include "../src/includes.h"

/*******************************************************************************************************
 * functional	:	git gpio vol level
 * argument in 	:	(temporary.vars_16b[0]) key
 * argument out :	(temporary.vars_16b[1])  
 * description	:	none
 * ****************************************************************************************************/
void Button_Read_lever(void)
{
    switch (temporary.vars_16b[0])
    {
    case BtnName_trimleft:
        if (PINS_NAME_TRIMLEFT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_trimright:
        if (PINS_NAME_TRIMRIGHT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_forwardkey:
        if (PINS_NAME_FORWARDKEY == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_backwardkey:
        if (PINS_NAME_BACKWARDKEY == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_steeringleft:
        if (PINS_NAME_STEERINGLEFT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_steeringright:
        if (PINS_NAME_STEERINGRIGHT == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    case BtnName_idchoose:
        if (PINS_NAME_IDCHOOSE == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;
    }
}

/*******************************************************************************************************
 * functional	:	read key
 * argument in 	:	(temporary.vars_8b[0]) step
 *                  (temporary.vars_8b[1]) shake
 *                  (temporary.vars_8b[2]) key
 * argument out :	(temporary.vars_8b[3]) btn_press or btn_null
 *                  (temporary.vars_8b[0]) step
 *                  (temporary.vars_8b[1]) shake
 * description	:	none
 * ****************************************************************************************************/
void Button_Read_Single(void)
{
    u8 valid = 0;

    temporary.vars_8b[3] = btn_null;

    switch (temporary.vars_8b[0])
    {
    case 0:                                           // detect
        temporary.vars_16b[0] = temporary.vars_8b[2]; // get key number
        Button_Read_lever();                          // read gpio level
        if (temporary.vars_16b[1] == 0)               // if level = 0, valid
        {                                             //
            temporary.vars_8b[1] = 0;                 // clear shake
            temporary.vars_8b[0]++;                   // step increase
        }
        break;

    case 1:                                           // shake
        temporary.vars_16b[0] = temporary.vars_8b[2]; // get key number
        Button_Read_lever();                          // read gpio level
        if (temporary.vars_16b[1] == 0)               // if level = 0, valid
        {                                             //
            temporary.vars_8b[1]++;                   // shake++
            if (temporary.vars_8b[1] > KEY_SHAKE_MAX) // pass
            {                                         //
                temporary.vars_8b[0]++;               // step increase
                temporary.vars_8b[1] = 0;             // clear shake
                temporary.vars_8b[3] = btn_press;     // return valid
            }                                         //
        }                                             //
        else                                          //
        {                                             // incorrect
            temporary.vars_8b[0] = 0;                 // step clear
        }
        break;

    case 2:                                             // wait key release
        temporary.vars_16b[0] = temporary.vars_8b[2];   // get key number
        Button_Read_lever();                            // read gpio level
        if (temporary.vars_16b[1] == 0)                 // if level = 0, valid
        {                                               //
            temporary.vars_8b[1] = 0;                   // clear shake
            temporary.vars_8b[3] = btn_press;           // return valid
        }                                               //
        else                                            //
        {                                               //
            temporary.vars_8b[1]++;                     // shake++
            if (temporary.vars_8b[1] > KEY_RELEASE_MIN) // reach long press threshold
            {                                           //
                temporary.vars_8b[3] = btn_null;        //
                temporary.vars_8b[1] = 0;               // clear shake
                temporary.vars_8b[0] = 0;               // clear step
            }
        }
        break;
    }
}

/*******************************************************************************************************
 * functional	:	user code enter
 * argument in 	:	none
 * argument out :	none
 * description	:	none
 * ****************************************************************************************************/
void Key_Scan_Function(void)
{
    if (dev_key.period > 0)
    {
        return;
    }
    dev_key.period = SYSTICE_MS * 5;

    //**************trimleft****************************
    temporary.vars_8b[0] = dev_key.trimleft.step;
    temporary.vars_8b[1] = dev_key.trimleft.shake;
    temporary.vars_8b[2] = BtnName_trimleft;
    Button_Read_Single();
    dev_key.trimleft.step = temporary.vars_8b[0];
    dev_key.trimleft.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_TRIMLEFT;
    else
        dev_key.state &= ~KEYBOARD_BITS_TRIMLEFT;

    //**************trimright****************************
    temporary.vars_8b[0] = dev_key.trimright.step;
    temporary.vars_8b[1] = dev_key.trimright.shake;
    temporary.vars_8b[2] = BtnName_trimright;
    Button_Read_Single();
    dev_key.trimright.step = temporary.vars_8b[0];
    dev_key.trimright.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_TRIMRIGHT;
    else
        dev_key.state &= ~KEYBOARD_BITS_TRIMRIGHT;

    //**************forwardkey****************************
    temporary.vars_8b[0] = dev_key.forwardkey.step;
    temporary.vars_8b[1] = dev_key.forwardkey.shake;
    temporary.vars_8b[2] = BtnName_forwardkey;
    Button_Read_Single();
    dev_key.forwardkey.step = temporary.vars_8b[0];
    dev_key.forwardkey.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_FORWARDKEY;
    else
        dev_key.state &= ~KEYBOARD_BITS_FORWARDKEY;

    //**************backwardkey****************************
    temporary.vars_8b[0] = dev_key.backwardkey.step;
    temporary.vars_8b[1] = dev_key.backwardkey.shake;
    temporary.vars_8b[2] = BtnName_backwardkey;
    Button_Read_Single();
    dev_key.backwardkey.step = temporary.vars_8b[0];
    dev_key.backwardkey.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_BACKWARDKEY;
    else
        dev_key.state &= ~KEYBOARD_BITS_BACKWARDKEY;

    //**************steeringleft****************************
    temporary.vars_8b[0] = dev_key.steeringleft.step;
    temporary.vars_8b[1] = dev_key.steeringleft.shake;
    temporary.vars_8b[2] = BtnName_steeringleft;
    Button_Read_Single();
    dev_key.steeringleft.step = temporary.vars_8b[0];
    dev_key.steeringleft.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_STEERINGLEFT;
    else
        dev_key.state &= ~KEYBOARD_BITS_STEERINGLEFT;

    //**************steeringright****************************
    temporary.vars_8b[0] = dev_key.steeringright.step;
    temporary.vars_8b[1] = dev_key.steeringright.shake;
    temporary.vars_8b[2] = BtnName_steeringright;
    Button_Read_Single();
    dev_key.steeringright.step = temporary.vars_8b[0];
    dev_key.steeringright.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_STEERINGRIGHT;
    else
        dev_key.state &= ~KEYBOARD_BITS_STEERINGRIGHT;

    //**************idchoose****************************
    temporary.vars_8b[0] = dev_key.idchoose.step;
    temporary.vars_8b[1] = dev_key.idchoose.shake;
    temporary.vars_8b[2] = BtnName_idchoose;
    Button_Read_Single();
    dev_key.idchoose.step = temporary.vars_8b[0];
    dev_key.idchoose.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_IDCHOOSE;
    else
        dev_key.state &= ~KEYBOARD_BITS_IDCHOOSE;

    //******************detect data send enable*****************
    if (dev_key.state & 0xfc) // any keys had pressed
    {                         //
        dev_sleep.tick = 0;   // clear tick amount
    } 
    dev_wave38k.data = dev_key.state;              // read keyboard value
    dev_wave38k.data <<= 8;                        // move to high 8 bit
    dev_wave38k.data |= (~dev_key.state) & 0x00ff; // reverse the data and move to low 8 bit
    dev_wave38k.statu |= PWM_WAVESEND_ENABLE;      // sign updata flag
}
