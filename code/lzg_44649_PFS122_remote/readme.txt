V0.01:  红外波形编码规则
	9ms (L) + 4.5ms(H) + 1byte(ADDR origion) + 1byte(ADDR reverse) + 1byte(Data origin) + 1byte(Data reverse)
	周期 67ms

V0.02:  红外波形编码规则
	4.5ms(L) + 2.3ms(H) + 1byte(Data)
	周期 19ms

V0.03:  数量量变更为双字节
	4.5ms(L) + 2.3ms(H) + 1byte(Data) + 1byte(data reverse)

	bit7:	trim		left
	bit6:	trim		right
	bit5:	forward		key
	bit4:	backward	key

	bit3:	steering	left
	bit2:	steering	right
	bit1:	idchoose
	bit0:

	数据响应之前，需要对应idchoose是否匹配。

V0.04: 在0.03的版本上，变更了IO口
	增加低电量显示
