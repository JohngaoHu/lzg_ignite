#ifndef _RAPIDRAGON_H_
#define _RAPIDRAGON_H_

/**********************************************************************
	.ADJUST_IC	SYSCLK=IHRC/4, IHRC=16MHz, VDD=5V;
***********************************************************************/

.ADJUST_IC	SYSCLK=IHRC/4, IHRC=16MHz, VDD=3V;

MemAddr_Base = _SYS(RAM_SIZE) - 1;
	while (MemAddr_Base$0--)
	{
		*MemAddr_Base = 0;
	}

#endif

