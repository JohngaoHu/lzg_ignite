#include "includes.h"

/***************************************************************************
 * function     :   calculate dynamic value with compensation parameter      
 * argument in  :   temporary.vars_8b[1]  origin dynamic
 * argument out :   
 * description  :   
 *                  only be used straight running
 *                  if compensation on right of middle location, it means left motor decrease dynamic
 *                  if compensation on left of middle location , it menas right motor need to decrease dynamic
 *                  calculate dual wheel maximum speed
 * *************************************************************************/
void Motor_Process_Compensation(void)
{
    if (motor_dev.compensation == CALIBRATION_CENTRE_DEFAULT)                           // Did not compensate
    {                                                                                   //
        motor_dev.dri_left.pwm = temporary.vars_8b[1];                                  //
        motor_dev.dri_right.pwm = temporary.vars_8b[1];                                 //
    }                                                                                   //
    else                                                                                // adjusted
    {                                                                                   //
        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)                        // need to decrease left dynamic
        {                                                                               //
            motor_dev.dri_right.pwm = temporary.vars_8b[1];                             // right full power
            temporary.vars_8b[2] = motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT; // calculate difference
            if (temporary.vars_8b[1] > temporary.vars_8b[2])                            // normally it is ture
            {                                                                           //
                temporary.vars_8b[1] = temporary.vars_8b[1] - temporary.vars_8b[2];     //
            }                                                                           //
            else                                                                        // normally can not run to here
            {                                                                           //
                temporary.vars_8b[1] = 0;                                               //
            }                                                                           //
            motor_dev.dri_left.pwm = temporary.vars_8b[1];                              //
        }                                                                               //
        else                                                                            //
        {                                                                               //
            motor_dev.dri_left.pwm = temporary.vars_8b[1];                              //
            temporary.vars_8b[2] = CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation; //
            if (temporary.vars_8b[1] > temporary.vars_8b[2])                            //
            {                                                                           //
                temporary.vars_8b[1] = temporary.vars_8b[1] - temporary.vars_8b[2];     //
            }                                                                           //
            else                                                                        //
            {                                                                           //
                temporary.vars_8b[1] = 0;                                               //
            }                                                                           //
            motor_dev.dri_right.pwm = temporary.vars_8b[1];                             //
        }                                                                               //
    }
}

/***************************************************************************
 * function     :   Write control parameter to dynamical system        
 * argument in  :   
 *                  temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 * argument out :    
 * description  :   50ms  
 * *************************************************************************/
#define STRAIGHT_SPEED_LOW 0
#define STRAIGHT_SPEED_HIGH 1
#define STRAIGHT_SPEED_SET STRAIGHT_SPEED_HIGH
#define ROUND_SPEED_LOW 0
#define ROUND_SPEED_HIGH 1
#define ROUND_SPEED_SET ROUND_SPEED_HIGH

#define MOTOR_CLIMB_TURNFAST 255
#define MOTOR_CLIMB_TURNSLOW 64
void Motor_Algorithm_Running(void)
{
    static u8 previous_pattern = DYNAMIC_DIR_STOP;

    if (motor_dev.compensation == 0)
    {
        motor_dev.compensation = CALIBRATION_CENTRE_DEFAULT;
    }

    switch (temporary.vars_8b[0])
    {
    case DYNAMIC_DIR_STOP:
        motor_dev.dri_left.direction = MOTOR_CMD_STOP;
        motor_dev.dri_right.direction = MOTOR_CMD_STOP;
        motor_dev.dri_left.pwm = 0;
        motor_dev.dri_right.pwm = 0;
        break;

    case DYNAMIC_DIR_FORWARD:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if ((previous_pattern == DYNAMIC_DIR_LEFTUP) || (previous_pattern == DYNAMIC_DIR_LEFT))
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_left.pwm = 200;
                motor_dev.dri_right.pwm = 50;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                Motor_Process_Compensation();
#if STRAIGHT_SPEED_SET == STRAIGHT_SPEED_LOW
                temporary.vars_8b[2] = motor_dev.dri_left.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_left.pwm >> 3;
                motor_dev.dri_left.pwm -= temporary.vars_8b[2];
                motor_dev.dri_left.pwm -= temporary.vars_8b[3];
                temporary.vars_8b[2] = motor_dev.dri_right.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_right.pwm >> 3;
                motor_dev.dri_right.pwm -= temporary.vars_8b[2];
                motor_dev.dri_right.pwm -= temporary.vars_8b[3];
#endif
            }
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
            Motor_Process_Compensation();
        }
        break;

    case DYNAMIC_DIR_REVERSE:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if ((previous_pattern == DYNAMIC_DIR_RIGHTDN) || (previous_pattern == DYNAMIC_DIR_RIGHT))
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_left.pwm = 50;
                motor_dev.dri_right.pwm = 200;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                Motor_Process_Compensation();
#if STRAIGHT_SPEED_SET == STRAIGHT_SPEED_LOW
                temporary.vars_8b[2] = motor_dev.dri_left.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_left.pwm >> 3;
                motor_dev.dri_left.pwm -= temporary.vars_8b[2];
                motor_dev.dri_left.pwm -= temporary.vars_8b[3];
                temporary.vars_8b[2] = motor_dev.dri_right.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_right.pwm >> 3;
                motor_dev.dri_right.pwm -= temporary.vars_8b[2];
                motor_dev.dri_right.pwm -= temporary.vars_8b[3];
#endif
            }
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
            Motor_Process_Compensation();
        }
        break;

    case DYNAMIC_DIR_LEFTUP:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if (previous_pattern == DYNAMIC_DIR_LEFT)
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_left.pwm = 255;
                motor_dev.dri_right.pwm = 90;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
#if ROUND_SPEED_SET == ROUND_SPEED_LOW
                motor_dev.dri_left.pwm = 50;
                motor_dev.dri_right.pwm = 90;
#elif ROUND_SPEED_SET == ROUND_SPEED_HIGH
                motor_dev.dri_left.pwm = 90;   //
                motor_dev.dri_right.pwm = 255; //
#endif
            }
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_left.pwm = MOTOR_CLIMB_TURNSLOW;
            motor_dev.dri_right.pwm = MOTOR_CLIMB_TURNFAST;
        }
        break;

    case DYNAMIC_DIR_RIGHTDN:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if (previous_pattern == DYNAMIC_DIR_RIGHT)
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_left.pwm = 90;
                motor_dev.dri_right.pwm = 255;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
#if ROUND_SPEED_SET == ROUND_SPEED_LOW
                motor_dev.dri_left.pwm = 90;
                motor_dev.dri_right.pwm = 50;
#elif ROUND_SPEED_SET == ROUND_SPEED_HIGH
                motor_dev.dri_left.pwm = 255;
                motor_dev.dri_right.pwm = 90;
#endif
            }
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_left.pwm = MOTOR_CLIMB_TURNFAST;
            motor_dev.dri_right.pwm = MOTOR_CLIMB_TURNSLOW;
        }
        break;

    case DYNAMIC_DIR_LEFTDN:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
#if ROUND_SPEED_SET == ROUND_SPEED_LOW
            motor_dev.dri_left.pwm = 50;
            motor_dev.dri_right.pwm = 90;
#elif ROUND_SPEED_SET == ROUND_SPEED_HIGH
            motor_dev.dri_left.pwm = 90;
            motor_dev.dri_right.pwm = 255;
#endif
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_left.pwm = MOTOR_CLIMB_TURNSLOW;
            motor_dev.dri_right.pwm = MOTOR_CLIMB_TURNFAST;
        }
        break;

    case DYNAMIC_DIR_RIGHTUP:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
#if ROUND_SPEED_SET == ROUND_SPEED_LOW
            motor_dev.dri_left.pwm = 90;
            motor_dev.dri_right.pwm = 50;
#elif ROUND_SPEED_SET == ROUND_SPEED_HIGH
            motor_dev.dri_left.pwm = 255;
            motor_dev.dri_right.pwm = 90;
#endif
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_left.pwm = MOTOR_CLIMB_TURNFAST;
            motor_dev.dri_right.pwm = MOTOR_CLIMB_TURNSLOW;
        }
        break;

    case DYNAMIC_DIR_LEFT:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
#if ROUND_SPEED_SET == ROUND_SPEED_LOW
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.pwm = 50;
            motor_dev.dri_right.pwm = 90;
        }
        else
        {
            motor_dev.dri_left.pwm = 100;
            motor_dev.dri_right.pwm = 200;
        }
#elif ROUND_SPEED_SET == ROUND_SPEED_HIGH
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.pwm = 100;
            motor_dev.dri_right.pwm = 180;
        }
        else
        {
            motor_dev.dri_left.pwm = 255;
            motor_dev.dri_right.pwm = 255;
        }
#endif
        break;

    case DYNAMIC_DIR_RIGHT:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
#if ROUND_SPEED_SET == ROUND_SPEED_LOW
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.pwm = 90;
            motor_dev.dri_right.pwm = 50;
        }
        else
        {
            motor_dev.dri_left.pwm = 200;
            motor_dev.dri_right.pwm = 100;
        }
#elif ROUND_SPEED_SET == ROUND_SPEED_HIGH
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.pwm = 180;
            motor_dev.dri_right.pwm = 100;
        }
        else
        {
            motor_dev.dri_left.pwm = 255;
            motor_dev.dri_right.pwm = 255;
        }
#endif
        break;
    }
    previous_pattern = temporary.vars_8b[0];
}

/***************************************************************************
 * function:        single byte analysis version
 * argument in:     none
 * argument out:    temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 *                  temporary.vars_8b[3]  0 success others fail
 * description:     
 * *************************************************************************/
void motor_analysis_ir_rec(void)
{
    //*****************verify packet format***************************************
    temporary.vars_8b[0] = (dev_infrared.dev_rec.data >> 5) & 0x1f;
    temporary.vars_8b[1] = dev_infrared.dev_rec.data & 0x1f;
    dev_infrared.dev_rec.data = 0;
    temporary.vars_8b[1] = (~temporary.vars_8b[1]) & 0x1f;
    if (temporary.vars_8b[0] != temporary.vars_8b[1])
    {
        temporary.vars_8b[3] = 0xff;
        return;
    }

    //*******************verify id***************************************************
#if REMODE_ID_VERIFY > 0
    if (temporary.vars_8b[0] & PACKET_BITS_ID)
    {
        if (GPIO_Pin_id_choose)
        {
            temporary.vars_8b[3] = 0xff;
            return;
        }
    }
    else // remote id option float
    {
        if (!GPIO_Pin_id_choose)
        {
            temporary.vars_8b[3] = 0xff;
            return;
        }
    }
#endif

    //*******************verify DATA***************************************************
    temporary.vars_8b[0] = (temporary.vars_8b[0] >> 1) & 0x0f;

    //**********************trim left operate******************************************
    if ((temporary.vars_8b[0] == Key_Result_TrimLe) ||
        (temporary.vars_8b[0] == Key_Result_UpTrimL) ||
        (temporary.vars_8b[0] == Key_Result_DnTrimL))
    {
        if (motor_dev.compensation < (COMPENSATION_RIGHT_MAX - COMPENSATION_ADJUST_EXTENT)) //
        {                                                                                   //
            motor_dev.compensation += COMPENSATION_ADJUST_EXTENT;                           //
        }                                                                                   //
        else                                                                                //
        {                                                                                   //
            motor_dev.compensation = COMPENSATION_RIGHT_MAX;                                //
        }
    }

    //**********************trim right operate******************************************
    if ((temporary.vars_8b[0] == Key_Result_TrimRi) ||
        (temporary.vars_8b[0] == Key_Result_UpTrimR) ||
        (temporary.vars_8b[0] == Key_Result_DnTrimR))
    {
        if (motor_dev.compensation > (COMPENSATION_LEFT_MIN + COMPENSATION_ADJUST_EXTENT)) //
        {                                                                                  //
            motor_dev.compensation -= COMPENSATION_ADJUST_EXTENT;                          //
        }                                                                                  //
        else                                                                               //
        {                                                                                  //
            motor_dev.compensation = COMPENSATION_LEFT_MIN;                                //
        }
    }

    switch (temporary.vars_8b[0])
    {
    case Key_Result_Null:
        temporary.vars_8b[0] = DYNAMIC_DIR_STOP;
        temporary.vars_8b[1] = 0;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_Up:
    case Key_Result_UpTrimL:
    case Key_Result_UpTrimR:
        temporary.vars_8b[0] = DYNAMIC_DIR_FORWARD;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_Dn:
    case Key_Result_DnTrimL:
    case Key_Result_DnTrimR:
        temporary.vars_8b[0] = DYNAMIC_DIR_REVERSE;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_Left:
        temporary.vars_8b[0] = DYNAMIC_DIR_LEFT;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_Right:
        temporary.vars_8b[0] = DYNAMIC_DIR_RIGHT;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_UpLeft:
        temporary.vars_8b[0] = DYNAMIC_DIR_LEFTUP;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_UpRight:
        temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTUP;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_DnLeft:
        temporary.vars_8b[0] = DYNAMIC_DIR_LEFTDN;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    case Key_Result_DnRight:
        temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTDN;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
        break;

    default:
        temporary.vars_8b[3] = 0xff;
        break;
    }
}

/***************************************************************************
 * function:        Analysis receive data
 * argument in:     none
 * argument out:    none
 * description:     10m
 * *************************************************************************/
void Motor_Analysis_IRrec(void)
{
    if ((bat_dev.status & BATTSTA_BIT_CHARGING) || (bat_dev.status & BATTSTA_BIT_INSUFFICIENT) || (motor_dev.status & MOTSTATUS_BITS_ERRSTALL))
    {
        dev_infrared.statu &= ~IR_REVSTATU_UPDATA;                         // if shall error occur, not do it ,it will be running after alarm display complish
        if ((motor_dev.dri_left.pwm > 0) || (motor_dev.dri_right.pwm > 0)) // motor running
        {                                                                  //
            temporary.vars_8b[0] = DYNAMIC_DIR_STOP;                       // motor stop
            temporary.vars_8b[1] = 0;                                      // clear dynamic
            Motor_Algorithm_Running();                                     // calculate pwm duty
            Motor_Operate_Function();                                      // take effect parameter
        }
    }
    else // car normally
    {
        if (dev_infrared.statu & IR_REVSTATU_UPDATA)   // receive ir signal
        {                                              //
            process.gotosleep_tim = 0;                 //
            motor_analysis_ir_rec();                   // analyze packet
            if (temporary.vars_8b[3] > 0)              // analysis incorrect
            {                                          //
                dev_infrared.offline++;                // increase offline counter
            }                                          //
            else                                       //
            {                                          //
                dev_infrared.offline = 0;              // clear offline counter
                Motor_Algorithm_Running();             //
                Motor_Operate_Function();              //
            }                                          //
            dev_infrared.statu &= ~IR_REVSTATU_UPDATA; // must be executed last
        }
        else                                                                   // no data was received
        {                                                                      //
            if ((motor_dev.dri_left.pwm > 0) || (motor_dev.dri_right.pwm > 0)) // present stage is running
            {                                                                  //
                dev_infrared.offline++;                                        // increase offline count
            }                                                                  //
        }

        if (dev_infrared.offline > 25)               // the abnormal time exceeds 250ms
        {                                            //
            dev_infrared.offline = 0;                //
            temporary.vars_8b[0] = DYNAMIC_DIR_STOP; //
            temporary.vars_8b[1] = 0;                //
            Motor_Algorithm_Running();               //
            Motor_Operate_Function();                //
        }                                            //
    }
}

/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     50ms
 * *************************************************************************/
void Motor_Task_device(void)
{
    if (motor_dev.period > 0)
    {
        return;
    }
    motor_dev.period = TICK_MS * 10;
    Motor_Analysis_IRrec();
}
