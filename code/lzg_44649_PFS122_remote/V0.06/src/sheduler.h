#ifndef _SHEDULER_H_
#define _SHEDULER_H_

#include "../src/typedef.h"

struct Scheduler_type
{
    u8 step;
    u8 per20ms;
    u8 per1s;
    u16 gotosleep_tim;
};
extern struct Scheduler_type process;



void Callback_event_task(void);
void Task_Process_Global(void);
#endif
