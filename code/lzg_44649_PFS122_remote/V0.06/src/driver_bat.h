#ifndef _DRIVER_BAT_H_
#define _DRIVER_BAT_H_
#include "typedef.h"

//--------8bit  ref3v  采样1/2--------------------
//--------VDD use ldo chip to stable voltage
#define BATADC_RESOLUTION8B_3V10 131
#define BATADC_RESOLUTION8B_3V18 135
#define BATADC_RESOLUTION8B_3V30 140
#define BATADC_RESOLUTION8B_4V57 195

//--------通过内部1.2V基准，反推VCC供电电压----------------
#define VDDADC_RESOLUTION8B_3V0    115


#define BATTSTA_BIT_STA (BIT5)          // is charging
#define BATTSTA_BIT_ALARM (BIT4)        // battery voltage low than 3.18V
#define BATTSTA_BIT_STEP (BIT3)         // temporary variables
#define BATTSTA_BIT_CHARGING (BIT2)     // charging stage
#define BATTSTA_BIT_CHARGEFULL (BIT1)   // charge completed
#define BATTSTA_BIT_INSUFFICIENT (BIT0) // power defected

struct Bat_CTR
{
    u16 period;
    u8 status;
    u8 shake;
    u8 discharge_step;
    u8 recover;
    u8 stage;
    u8 detect;

    u8 fullsta;
};

void Bat_Task_Driver(void);
#endif
