#ifndef _DRIVER_MOTOR_H_
#define _DRIVER_MOTOR_H_

#include "../src/typedef.h"

#define IR_Receive_PowerOn GPIO_Pin_pwrctr = 1
#define IR_Receive_PowerOff GPIO_Pin_pwrctr = 0

#define ANALYSIS_RECBYTE_1VERSION 0
#define ANALYSIS_RECBYTE_2VERSION 1
#define ANALYSIS_RECBYTE_CONFIG ANALYSIS_RECBYTE_2VERSION

/* 
//==============  original parameters =========================
#define COMPENSATION_LEFT_MIN 12
#define CALIBRATION_CENTRE_DEFAULT 0xff
#define COMPENSATION_RIGHT_MAX 500 
*/

#define COMPENSATION_LEFT_MIN 155
#define CALIBRATION_CENTRE_DEFAULT 0xff
#define COMPENSATION_RIGHT_MAX 355

#define COMPENSATION_ADJUST_EXTENT 10
#define MOTOR_SPEED_CONFIG 255

enum
{
    MOTOR_CMD_STOP = 0,
    MOTOR_CMD_FOREWARD,
    MOTOR_CMD_REVERSE,
};

enum
{
    DYNAMIC_DIR_STOP = 0,
    DYNAMIC_DIR_FORWARD = 0xc1,
    DYNAMIC_DIR_REVERSE,
    DYNAMIC_DIR_LEFT,
    DYNAMIC_DIR_RIGHT,
    DYNAMIC_DIR_LEFTUP,
    DYNAMIC_DIR_LEFTDN,
    DYNAMIC_DIR_RIGHTUP,
    DYNAMIC_DIR_RIGHTDN,
};


enum
{
    Key_Result_Null = 0,
    Key_Result_Up,
    Key_Result_Dn,
    Key_Result_Left,
    Key_Result_Right,
    Key_Result_TrimLe,
    Key_Result_TrimRi,
    Key_Result_UpTrimL,
    Key_Result_UpTrimR,
    Key_Result_UpLeft,
    Key_Result_UpRight,
    Key_Result_DnTrimL,
    Key_Result_DnTrimR,
    Key_Result_DnLeft,
    Key_Result_DnRight,
};

#define MOTSTATUS_BITS_ERRSTALL (1 << 7)
#define MOTSTATUS_BITS_LEFTDIR (1 << 6)
#define MOTSTATUS_BITS_RIGHTDRI (1 << 6)

struct Motor_Type
{
    u16 period;
    u8 status;
    u16 error;
    u16 compensation;

    struct MOTL_LEFT
    {
        u16 last_error;
        u8 stall;
        u8 direction;
        u8 pwm;
        u8 previous_dir;
    } dri_left;

    struct MOTR_RIGHT
    {
        u16 last_error;
        u8 stall;
        u8 direction;
        u8 pwm;
        u8 previous_dir;
    } dri_right;
};

void Motor_Task_device(void);
void Motor_Calculate_Parameter(void);
void Motor_Execute_Device(void);
void Motor_Analysis_IRrec(void);
#endif
