#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include "../src/typedef.h"
#include "../src/sheduler.h"
#include "../src/driver_led.h"
#include "../src/device_motor.h"
#include "../src/device_ir.h"
#include "../src/driver_adc.h"
#include "../src/device_backoffice.h"
#include "../src/driver_bat.h"


#define   gotosleep_timover_size     300      //unit: second
/*
    Note:
        This set of variables is not allowed to be called during interrupts
        its greatest use is to simulate calls to functions with arguments
*/
struct temporary_type //临时变量
{
    u8 vars_8b[4];
    u16 vars_16b[4];
};

extern struct temporary_type temporary;
void Board_Initialize_Global(void);
void Callback_Sofetim_20ms(void);
void Sleep_Read_Statu(void);

/*
*************************************
测试发现，变量必须定义在此处，若定义在C文件中，编译发现RAM占用明显小，说明IDE未分配内存
*************************************
*/
.ramadr 0x00 
uint16_t MemAddr_Base; //在作内存清空的时候，指向内存的指针变量，必须是一个word变量，同时其指针小于0xfe
struct Scheduler_type process;    //调度器
struct temporary_type temporary;  //中间变量，切不可在中断中引用
struct LED_CTR led_dev;           //灯光控制器
struct Motor_Type motor_dev;      //马达控制器
struct ADC_CTR adc_dev;           //ADC控制器
struct Modescan_CTR modescan_dev; //模式控制器
struct Bat_CTR bat_dev;           //电池控制器
struct Infrared_Type dev_infrared;
#endif
