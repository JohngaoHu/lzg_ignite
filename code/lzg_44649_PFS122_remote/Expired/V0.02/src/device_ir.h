#ifndef _DEVICE_IR_H_
#define _DEVICE_IR_H_

#include "typedef.h"

#define IRKEY_BIT_FORWARD    (1 << 7)
#define IRKEY_BIT_REVERSE    (1 << 6)
#define IRKEY_BIT_RIGHT      (1 << 5)
#define IRKEY_BIT_LEFT       (1 << 4)
#define IRKEY_BIT_FINER      (1 << 3)
#define IRKEY_BIT_FINEL      (1 << 2)
#define IRKEY_BIT_ACTIVE     (1 << 1)

#define IR_REVSTATU_UPDATA  (1 << 7)
struct Infrared_Type{
    u8 statu;
    u8 step;
    u16 count;
    u8 offline;

    struct Receiv_Type{
        u32 data;
        u8 bits;
    }dev_rec;
};

void Infrared_Read_Signal(void);
void IR_Reorganize_Datarecd(void);
#endif
