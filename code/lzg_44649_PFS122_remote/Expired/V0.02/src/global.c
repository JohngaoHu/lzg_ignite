#include "../src/includes.h"

/***************************************************************************
 * function:        initialize t16
 * argument in:     none
 * argument out:    none
 * description:     1ms
 * *************************************************************************/
void T16_Initialize_Irq(void)
{
    $ t16m SYSCLK, / 4, BIT8; // 2us/step @2M, 1us/step @4M  BIT8含义是比较数值为8位，最大255溢出
}

/***************************************************************************
 * function:        initialize all io
 * argument in:     none
 * argument out:    none
 * description:     1 OUT  0 IN
 * *************************************************************************/
void Global_Initialize_GPIO(void)
{
    /*
    bit	GPIO_Pin_pwrctr:  PA.0
    bit	GPIO_Pin_pa1:  PA.1
    bit	GPIO_Pin_pa2:  PA.2
    bit	GPIO_Pin_ir_ba_data:  PA.3
    bit	GPIO_Pin_batdetect:  PA.4
    bit	GPIO_Pin_ir_lf_data:  PA.5
    bit	GPIO_Pin_modein:  PA.6
    bit	GPIO_Pin_ch_sta:  PA.7
    */
#ifndef DEBUG_LOG_EN
    padier = 11101000b; // 1 = dg-in & wakeup
    pa = 00000000b;     // port
    pac = 00000111b;    // direction 0 = in
    paph = 10101000b;   // pull up resistor 1 = ph
#else
    //-------PA6作为UART IO---------------------
    padier = 10101000b; // 1 = dg-in & wakeup
    pa = 01000000b;     // port
    pac = 01000111b;    // direction 0 = in
    paph = 10101000b;   // pull up resistor 1 = ph
#endif

    /*
    bit	GPIO_Pin_Motlcur:  PB.0
    bit	GPIO_Pin_Motrcur:  PB.1
    bit	GPIO_Pin_motRpwmreverse:  PB.2
    bit	GPIO_Pin_led:  PB.3
    bit	GPIO_Pin_motRpwmforward:  PB.4
    bit	GPIO_Pin_motLpwmforward:  PB.5
    bit	GPIO_Pin_motLpwmreverse:  PB.6
    bit	GPIO_Pin_ir_rf_data:  PB.7
    */
    pbdier = 10000000b; // 1 = dg-in & wakeup
    pb = 00001000b;     // port
    pbc = 01111100b;    // direction 0 = in
    pbph = 10000000b;   // pull up resistor 1 = ph

    IR_Receive_PowerOn;
    LED_Turn_Off;
    MotorIO_Left_Fshut();
    MotorIO_Left_Rshut();
    MotorIO_Right_Fshut();
    MotorIO_Right_Rshut();
}

/***************************************************************************
 * function:        initialize peripheral equipment
 * argument in:     none
 * argument out:    none
 * description:
 * *************************************************************************/
void Board_Initialize_Global(void)
{
    clkmd = 00010000b;        //IHRC/4,wdt dis, PA5 as IO
    intrq = 0x00;             //
    eoscr = 0x00;             //bandgap & lvr: normal
    Global_Initialize_GPIO(); //configure gpio
    T16_Initialize_Irq();     //configure tim 16
    inten |= (1 << 2);        //enable tm16
    inten |= (1 << 3);        //enable adc irq
    intrq = 0x00;             //clear all interrupt flag
    GLOBAL_INTERRUPT_ENABLE;  //enable global interrupt sw

    temporary.mem16b[2] = LED_Stage_Tunron;
    LED_Write_State();
}

/*************************************************************************
 * function :       产生20ms软定时器
 * argument in:     none
 * argument out:    none
 * description:     由定时器中断产生 100us
 * ***********************************************************************/
void Callback_Sofetim_20ms(void)
{
    process.per20ms++;
    //--------------20ms complete------------------
    if (process.per20ms >= 200)
    {
        process.per20ms = 0;

        //stall counter
        motor_dev.dri_left.stall++;
        motor_dev.dri_right.stall++;

        //--------Second counter-----------------------
        process.per1s++;
        if (process.per1s >= 50)
        {
            process.per1s = 0;
            process.gotosleep_tim++;
        }
    }
}

/*************************************************************************
 * function :       低功耗模式
 * argument in:     none
 * argument out:    none
 * description:     dir0 in  dir1 output
 * ***********************************************************************/
void Sleep_Read_Statu(void)
{
    temporary.mem16b[2] = 0;
    while (temporary.mem16b[2] == 0)
    {
        if (process.gotosleep_tim >= gotosleep_timover_size) // no signal received and time over
        {
            process.gotosleep_tim = 0;
            temporary.mem16b[2]++;
            continue;
        }

        if (bat_dev.status & BATTSTA_BIT_INSUFFICIENT) //battery voltage too low
        {
            temporary.mem16b[2]++;
            continue;
        }

        break; //arrived here, it means everytings is ok
    }
    if (temporary.mem16b[2] == 0) // nothing occured ,get away here
    {
        return;
    }
/*
    Note:
        if it is already in sleep mode,there are two ways as below to wake it up.
        1. Plug in the usb charging cable
        2. push the switch into wall climing mode
*/
#ifdef OPTIONAL_SLEEP_EN
    disgint;      //disable global interrupt
    inten = 0x00; //operate coressponding channal interrupt
    eoscr = 0x01; //off external oscillator && bandgap power down
    adcc = 0x00;  //off adc covertor

    /*
    bit	GPIO_Pin_pwrctr:  PA.0
    bit	GPIO_Pin_pa1:  PA.1
    bit	GPIO_Pin_pa2:  PA.2
    bit	GPIO_Pin_ir_ba_data:  PA.3
    bit	GPIO_Pin_batdetect:  PA.4
    bit	GPIO_Pin_ir_lf_data:  PA.5
    bit	GPIO_Pin_modein:  PA.6
    bit	GPIO_Pin_ch_sta:  PA.7
    */
    padier = 0xc0; //PA7 PA6 wakeup
    pa = 0x01;     // port
    pac = 0x07;    // direction 0 = in
    paph = 0x80;   // pa7 pull up, pa6 no allow pull up

    /*
    bit	GPIO_Pin_Motlcur:  PB.0
    bit	GPIO_Pin_Motrcur:  PB.1
    bit	GPIO_Pin_motRpwmreverse:  PB.2
    bit	GPIO_Pin_led:  PB.3
    bit	GPIO_Pin_motRpwmforward:  PB.4
    bit	GPIO_Pin_motLpwmforward:  PB.5
    bit	GPIO_Pin_motLpwmreverse:  PB.6
    bit	GPIO_Pin_ir_rf_data:  PB.7
    */
    pbdier = 0x00; //no wake up
    pb = 0x08;     // port
    pbc = 0x7c;    // direction 0 = in
    pbph = 0x00;   // pull up resistor 1 = ph

    LED_Turn_Off;
    IR_Receive_PowerOff;
    Motor_Close_Double();

    stopsys;
    process.step = 0;
#endif
}
