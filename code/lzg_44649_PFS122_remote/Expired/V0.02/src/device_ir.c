#include "includes.h"

#define IR_HANDLEN_4MS 32
#define IR_HANDLEN_2MS 16
#define IR_SIGNAL_BIT1 10
/***************************************************************************
 * function:        IR Receiving Sensor
 * argument in:     none
 * argument out:    none
 * description:     it located in the interrupt function, becasue its execution time is short
 * *************************************************************************/
void Infrared_Read_Signal(void)
{
   if (dev_infrared.statu & IR_REVSTATU_UPDATA)
    {
        dev_infrared.step = 0xff;
    }

    switch (dev_infrared.step)
    {
    case 0:
        if (!PIN_INFRARED_REC)
        {
            dev_infrared.step++;
        }
        break;

    case 1:
        if (!PIN_INFRARED_REC)
        {
            dev_infrared.count++;
        }
        else
        {
            if (dev_infrared.count > IR_HANDLEN_4MS)
            {
                dev_infrared.step++;
            }
            else
            {
                dev_infrared.step = 0;
            }
            dev_infrared.count = 0;
        }
        break;

    case 2:
        if (PIN_INFRARED_REC)
        {
            dev_infrared.count++;
        }
        else
        {
            if (dev_infrared.count > IR_HANDLEN_2MS)
            {
                dev_infrared.step++;
                dev_infrared.dev_rec.bits = 0;
                dev_infrared.dev_rec.data = 0;
            }
            else
            {
                dev_infrared.step = 0;
            }
            dev_infrared.count = 0;
        }
        break;

    case 3: 
        if (PIN_INFRARED_REC)
        {
            dev_infrared.step++;
            dev_infrared.count = 0;
        }
        else
        {
            dev_infrared.count++;
            if (dev_infrared.count > IR_HANDLEN_2MS)
            {
                dev_infrared.step = 0xff;
            }
        }
        break;

    case 4:
        if (PIN_INFRARED_REC)
        {
            dev_infrared.count++;
            if (dev_infrared.count > IR_HANDLEN_2MS)
            {
                dev_infrared.step = 0xff;
            }
        }
        else
        {
            dev_infrared.dev_rec.data <<= 1;
            if (dev_infrared.count > IR_SIGNAL_BIT1)
            {
                dev_infrared.dev_rec.data |= 0x01;
            }

            dev_infrared.dev_rec.bits++;
            if (dev_infrared.dev_rec.bits == 8)
            {
                dev_infrared.step = 88;
            }
            else
            {
                dev_infrared.step -= 1;
            }
            dev_infrared.count = 0;
        }
        break;

    case 88:
        dev_infrared.statu |= IR_REVSTATU_UPDATA;
        dev_infrared.step = 0;
        dev_infrared.count = 0;
        dev_infrared.dev_rec.bits = 0;
        break;

    case 0xff:
        dev_infrared.step = 0;
        dev_infrared.count = 0;
        dev_infrared.dev_rec.bits = 0;
        break;
    }
}

