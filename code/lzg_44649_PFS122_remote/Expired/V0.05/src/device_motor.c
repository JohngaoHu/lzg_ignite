#include "includes.h"

/***************************************************************************
 * function     :   calculate dynamic value with compensation parameter      
 * argument in  :   temporary.vars_8b[1]  origin dynamic
 * argument out :   
 * description  :   
 *                  only be used straight running
 *                  if compensation on right of middle location, it means left motor decrease dynamic
 *                  if compensation on left of middle location , it menas right motor need to decrease dynamic
 *                  calculate dual wheel maximum speed
 * *************************************************************************/
void Motor_Process_Compensation(void)
{
    if (motor_dev.compensation == CALIBRATION_CENTRE_DEFAULT)
    {
        motor_dev.dri_left.pwm = temporary.vars_8b[1];
        motor_dev.dri_right.pwm = temporary.vars_8b[1];
    }
    else
    {
        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)                        // need to decrease left dynamic
        {                                                                               //
            motor_dev.dri_right.pwm = temporary.vars_8b[1];                             // right full power
            temporary.vars_8b[2] = motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT; // calculate difference
            if (temporary.vars_8b[1] > temporary.vars_8b[2])                            // normally it is ture
            {                                                                           //
                temporary.vars_8b[1] = temporary.vars_8b[1] - temporary.vars_8b[2];     //
            }                                                                           //
            else                                                                        // normally can not run to here
            {                                                                           //
                temporary.vars_8b[1] = 0;                                               //
            }                                                                           //
            motor_dev.dri_left.pwm = temporary.vars_8b[1];                              //
        }                                                                               //
        else
        {
            motor_dev.dri_left.pwm = temporary.vars_8b[1];
            temporary.vars_8b[2] = CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation;
            if (temporary.vars_8b[1] > temporary.vars_8b[2])
            {
                temporary.vars_8b[1] = temporary.vars_8b[1] - temporary.vars_8b[2];
            }
            else
            {
                temporary.vars_8b[1] = 0;
            }
            motor_dev.dri_right.pwm = temporary.vars_8b[1];
        }
    }
}

/***************************************************************************
 * function     :   Write control parameter to dynamical system        
 * argument in  :   
 *                  temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 * argument out :    
 * description  :   50ms  
 * *************************************************************************/
#define STEER_SHART_WALK 2
#define STEER_SHARP_CLIMB 3
void Motor_Algorithm_Running(void)
{
    static u8 previous_pattern = DYNAMIC_DIR_STOP;
    static u8 previous_step = 0;

    if (motor_dev.compensation == 0)
    {
        motor_dev.compensation = CALIBRATION_CENTRE_DEFAULT;
    }

    switch (temporary.vars_8b[0])
    {
    case DYNAMIC_DIR_STOP:
        motor_dev.dri_left.direction = MOTOR_CMD_STOP;
        motor_dev.dri_right.direction = MOTOR_CMD_STOP;
        motor_dev.dri_left.pwm = 0;
        motor_dev.dri_right.pwm = 0;
        previous_pattern = temporary.vars_8b[0];
        break;

    case DYNAMIC_DIR_FORWARD:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if ((previous_pattern == DYNAMIC_DIR_LEFTUP) || (previous_pattern == DYNAMIC_DIR_LEFT))
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_left.pwm = 200;
                motor_dev.dri_right.pwm = 50;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                Motor_Process_Compensation();
                temporary.vars_8b[2] = motor_dev.dri_left.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_left.pwm >> 3;
                motor_dev.dri_left.pwm -= temporary.vars_8b[2];
                motor_dev.dri_left.pwm -= temporary.vars_8b[3];
                temporary.vars_8b[2] = motor_dev.dri_right.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_right.pwm >> 3;
                motor_dev.dri_right.pwm -= temporary.vars_8b[2];
                motor_dev.dri_right.pwm -= temporary.vars_8b[3];
            }
            previous_pattern = temporary.vars_8b[0];
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
            Motor_Process_Compensation();
        }

        break;

    case DYNAMIC_DIR_REVERSE:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if ((previous_pattern == DYNAMIC_DIR_RIGHTDN) || (previous_pattern == DYNAMIC_DIR_RIGHT))
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_left.pwm = 50;
                motor_dev.dri_right.pwm = 200;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                Motor_Process_Compensation();
                temporary.vars_8b[2] = motor_dev.dri_left.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_left.pwm >> 3;
                motor_dev.dri_left.pwm -= temporary.vars_8b[2];
                motor_dev.dri_left.pwm -= temporary.vars_8b[3];
                temporary.vars_8b[2] = motor_dev.dri_right.pwm >> 2;
                temporary.vars_8b[3] = motor_dev.dri_right.pwm >> 3;
                motor_dev.dri_right.pwm -= temporary.vars_8b[2];
                motor_dev.dri_right.pwm -= temporary.vars_8b[3];
            }
            previous_pattern = temporary.vars_8b[0];
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
            Motor_Process_Compensation();
        }
        break;

    case DYNAMIC_DIR_LEFTUP:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if (previous_pattern == DYNAMIC_DIR_LEFT)
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_left.pwm = 255;
                motor_dev.dri_right.pwm = 127;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_left.pwm = 50;
                motor_dev.dri_right.pwm = 90;
            }
            previous_pattern = temporary.vars_8b[0];
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
            motor_dev.dri_right.pwm = 0xff;
            motor_dev.dri_left.pwm = 32;
        }
        break;

    case DYNAMIC_DIR_RIGHTDN:
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            if (previous_pattern == DYNAMIC_DIR_RIGHT)
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
                motor_dev.dri_left.pwm = 127;
                motor_dev.dri_right.pwm = 255;
            }
            else
            {
                motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
                motor_dev.dri_left.pwm = 90;
                motor_dev.dri_right.pwm = 50;
            }
            previous_pattern = temporary.vars_8b[0];
        }
        else
        {
            motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
            motor_dev.dri_left.pwm = 255;
            motor_dev.dri_right.pwm = 32;
        }
        break;

    case DYNAMIC_DIR_LEFTDN:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.pwm = 50;
            motor_dev.dri_right.pwm = 90;
        }
        else
        {
            motor_dev.dri_left.pwm = 32;
            motor_dev.dri_right.pwm = 255;
        }
        break;

    case DYNAMIC_DIR_RIGHTUP:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_left.pwm = 90;
            motor_dev.dri_right.pwm = 50;
        }
        else
        {
            motor_dev.dri_left.pwm = 255;
            motor_dev.dri_right.pwm = 32;
        }
        break;

    case DYNAMIC_DIR_LEFT:
        previous_pattern = temporary.vars_8b[0];
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_right.pwm = 90;
            motor_dev.dri_left.pwm = 50;
        }
        else
        {
            motor_dev.dri_right.pwm = 200;
            motor_dev.dri_left.pwm = 100;
        }
        break;

    case DYNAMIC_DIR_RIGHT:
        previous_pattern = temporary.vars_8b[0];
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        if (modescan_dev.mode == Running_Walk_Pattern)
        {
            motor_dev.dri_right.pwm = 50;
            motor_dev.dri_left.pwm = 90;
        }
        else
        {
            motor_dev.dri_right.pwm = 100;
            motor_dev.dri_left.pwm = 200;
        }
        break;
    }
}

/***************************************************************************
 * function:        single byte analysis version
 * argument in:     none
 * argument out:    temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 *                  temporary.vars_8b[3]  0 success others fail
 * description:     
 * *************************************************************************/
void _analysis_receive_version2Byte(void)
{
    temporary.vars_8b[0] = dev_infrared.dev_rec.data >> 8;    // achieve high 8bit
    temporary.vars_8b[1] = dev_infrared.dev_rec.data;         // achieve low  8bit, verify data
    temporary.vars_8b[2] = temporary.vars_8b[0] ^ SECRET_KEY; // calculate secret number
    dev_infrared.dev_rec.data = 0;                            //
    if (temporary.vars_8b[1] != temporary.vars_8b[2])         // verify data incorrect
    {
        temporary.vars_8b[3] = 0xff;
        return;
    }

#if REMODE_ID_VERIFY > 0
    if (temporary.vars_8b[0] & KEYBOARD_BITS_IDCHOOSE) // remote id option chosen
    {
        if (GPIO_Pin_id_choose)
        {
            temporary.vars_8b[3] = 0xff;
            return;
        }
    }
    else // remote id option float
    {
        if (!GPIO_Pin_id_choose)
        {
            temporary.vars_8b[3] = 0xff;
            return;
        }
    }
#endif

    if (temporary.vars_8b[0] & KEYBOARD_BITS_TRIMRIGHT)                                    // fine turning right
    {                                                                                      //
        if (motor_dev.compensation > (COMPENSATION_LEFT_MIN + COMPENSATION_ADJUST_EXTENT)) //
        {                                                                                  //
            motor_dev.compensation -= COMPENSATION_ADJUST_EXTENT;                          //
        }                                                                                  //
        else                                                                               //
        {                                                                                  //
            motor_dev.compensation = COMPENSATION_LEFT_MIN;                                //
        }                                                                                  //
    }                                                                                      //

    if (temporary.vars_8b[0] & KEYBOARD_BITS_TRIMLEFT)                                      // fine turning left
    {                                                                                       //
        if (motor_dev.compensation < (COMPENSATION_RIGHT_MAX - COMPENSATION_ADJUST_EXTENT)) //
        {                                                                                   //
            motor_dev.compensation += COMPENSATION_ADJUST_EXTENT;                           //
        }                                                                                   //
        else                                                                                //
        {                                                                                   //
            motor_dev.compensation = COMPENSATION_RIGHT_MAX;                                //
        }                                                                                   //
    }                                                                                       //

    temporary.vars_8b[0] &= 0xf0;

    if (temporary.vars_8b[0] == 0)
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_STOP;
        temporary.vars_8b[1] = 0;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == KEYBOARD_BITS_FORWARDKEY)
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_FORWARD;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == KEYBOARD_BITS_BACKWARDKEY)
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_REVERSE;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == KEYBOARD_BITS_STEERINGLEFT)
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_LEFT;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == KEYBOARD_BITS_STEERINGRIGHT)
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_RIGHT;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_STEERINGLEFT))
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_LEFTUP;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == (KEYBOARD_BITS_FORWARDKEY | KEYBOARD_BITS_STEERINGRIGHT))
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTUP;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGLEFT))
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_LEFTDN;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else if (temporary.vars_8b[0] == (KEYBOARD_BITS_BACKWARDKEY | KEYBOARD_BITS_STEERINGRIGHT))
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTDN;
        temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
        temporary.vars_8b[3] = 0;
    }
    else
    {
        temporary.vars_8b[3] = 0xff;
    }
}

/***************************************************************************
 * function:        Analysis receive data
 * argument in:     none
 * argument out:    none
 * description:     50m
 * *************************************************************************/
void Motor_Analysis_IRrec(void)
{
    if ((bat_dev.status & BATTSTA_BIT_CHARGING) ||     // battery charging stage
        (bat_dev.status & BATTSTA_BIT_INSUFFICIENT) || // battery insufficient stage
        (motor_dev.status & MOTSTATUS_BITS_ERRSTALL))  // motor protect stage
    {                                                  //
        temporary.vars_8b[0] = DYNAMIC_DIR_STOP;       // motor stop
        temporary.vars_8b[1] = 0;                      // clear ir receive flag ,
        dev_infrared.statu &= ~IR_REVSTATU_UPDATA;     // if shall error occur, not do it ,it will be running after alarm display complish
    }
    else // car normally
    {
        if (dev_infrared.statu & IR_REVSTATU_UPDATA) // receive ir signal
        {
            process.gotosleep_tim = 0;
            _analysis_receive_version2Byte();
            if (temporary.vars_8b[3] > 0) //analysis incorrect
            {
                dev_infrared.offline++;
            }
            else
            {
                dev_infrared.offline = 0;
            }
            dev_infrared.statu &= ~IR_REVSTATU_UPDATA;
        }
        else                                                                   // no data was received
        {                                                                      //
            if ((motor_dev.dri_left.pwm > 0) || (motor_dev.dri_right.pwm > 0)) // present stage is running
            {                                                                  //
                dev_infrared.offline++;                                        // increase offline count
            }                                                                  //
        }

        if (dev_infrared.offline > 10)
        {
            dev_infrared.offline = 0;
            temporary.vars_8b[0] = DYNAMIC_DIR_STOP;
            temporary.vars_8b[1] = 0;
        }
    }
    Motor_Algorithm_Running();
}

/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     50ms
 * *************************************************************************/
void Motor_Task_device(void)
{
    if (motor_dev.period > 0)
    {
        return;
    }
    motor_dev.period = TICK_MS * 50;
    Motor_Analysis_IRrec();
    Motor_Operate_Function();
}
