#include "includes.h"

/***************************************************************************
 * function:        scan key
 * argument in:     none
 * argument out:    none
 * description:     5ms
 *                  normal  (io == 1  mode = 0)
 * *************************************************************************/
void Modekey_Read_driver(void)
{
#if DEBUG_LOG_EN == 0
    switch (modescan_dev.step)
    {
    case 0:                                            //during normal, occured pin level change event
        if (modescan_dev.mode == Running_Walk_Pattern) //正常情况，mode0 走地模式， IO为高电平
        {                                              //
            if (!GPIO_Pin_modein)                      //检测到电平变低
            {                                          //
                modescan_dev.step = 1;                 //跳到1号房，作进一步检查
                modescan_dev.shake = 0;                //检查前，防抖值清空
            }                                          //
        }                                              //
        else                                           //正常情况下，mode1 爬墙模式， IO为低电平
        {                                              //
            process.gotosleep_tim = 0;                 //爬墙模式下，不允许休眠，SO 清空休眠计时
            if (GPIO_Pin_modein)                       //检测到电平变高
            {                                          //
                modescan_dev.step = 2;                 //跳到2号房，作进一步检查
                modescan_dev.shake = 0;                //
            }                                          //
        }                                              //
        break;

    case 1:                                                //mode == 0 && io == 0
        if (!GPIO_Pin_modein)                              //电平还是变为低
        {                                                  //
            modescan_dev.shake++;                          //计时
            if (modescan_dev.shake >= 10)                  //10个计时周期
            {                                              //
                modescan_dev.shake = 0;                    //清计数
                modescan_dev.mode = Running_Climb_Pattern; //更新模式
                modescan_dev.step = 0;                     //退出房间，回到原处
            }                                              //
        }                                                  //
        else                                               //
        {                                                  //在防抖期内，检测到高电平，丢弃事件
            modescan_dev.step = 0;                         //error, ignore
        }                                                  //
        break;

    case 2:                                               //mode == 1 &&  io == 1
        if (GPIO_Pin_modein)                              //电平还是变为高
        {                                                 //
            modescan_dev.shake++;                         //计时
            if (modescan_dev.shake >= 10)                 //10个计时周期
            {                                             //
                modescan_dev.shake = 0;                   //清计数
                modescan_dev.mode = Running_Walk_Pattern; //更新模式
                modescan_dev.step = 0;                    //退出房间，回到原处
            }                                             //
        }                                                 //
        else                                              //在防抖期内，检测到高电平，丢弃事件
        {                                                 //time not enough
            modescan_dev.step = 0;                        //error, ignore
        }
        break;
    }
#endif
}

/***************************************************************************
 * function:        motor stalled detect
 * argument in:     none
 * argument out:    none
 * description:     5ms
 * *************************************************************************/
void Motor_Detect_Stalled(void)
{
    if (motor_dev.status & MOTSTATUS_BITS_ERRSTALL)
    {
        motor_dev.dri_left.stall = 0;
        motor_dev.dri_right.stall = 0;
        return;
    }

    if ((bat_dev.status & BATTSTA_BIT_ALARM) || (bat_dev.status & BATTSTA_BIT_INSUFFICIENT))
    {
        return;
    }

    if (modescan_dev.mode)                            // climb mode
    {                                                 //
        temporary.vars_8b[0] = STALL_THRESHOLD_CLIMP; // ADC current over threshold
        temporary.vars_8b[1] = STALL_SHAKE_CLIMP;     // debounce
    }                                                 //
    else                                              // walk mode
    {                                                 //
        temporary.vars_8b[0] = STALL_THRESHOLD_GOUND; // ADC current over threshold
        temporary.vars_8b[1] = STALL_SHAKE_GOUND;     // debounce
    }                                                 //

    if (adc_dev.adc_motl.average < temporary.vars_8b[0])
    {
        motor_dev.dri_left.stall = 0; // increased in softtimer per 20ms
    }

    if (adc_dev.adc_motr.average < temporary.vars_8b[0])
    {
        motor_dev.dri_right.stall = 0; // increased in softtimer per 20ms
    }

    if ((motor_dev.dri_left.stall > temporary.vars_8b[1]) ||
        (motor_dev.dri_right.stall > temporary.vars_8b[1]))
    {
        motor_dev.status |= MOTSTATUS_BITS_ERRSTALL;
        temporary.vars_16b[2] = LED_Stage_GlintQuick;
        LED_Write_State();
    }
}

/***************************************************************************
 * function:        task for backoffice
 * argument in:     none
 * argument out:    none
 * description:     5ms
 * *************************************************************************/
void Backoffice_Task_Manage(void)
{
    if (modescan_dev.period > 0)
    {
        return;
    }
    modescan_dev.period = TICK_MS * 5;

    Modekey_Read_driver();
    Motor_Detect_Stalled();
}
