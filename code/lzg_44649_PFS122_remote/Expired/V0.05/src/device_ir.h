#ifndef _DEVICE_IR_H_
#define _DEVICE_IR_H_

#include "typedef.h"

#define KEYBOARD_BITS_STEERINGRIGHT (BIT7)
#define KEYBOARD_BITS_STEERINGLEFT (BIT6)
#define KEYBOARD_BITS_FORWARDKEY (BIT5)
#define KEYBOARD_BITS_BACKWARDKEY (BIT4)
#define KEYBOARD_BITS_TRIMRIGHT (BIT3)
#define KEYBOARD_BITS_TRIMLEFT (BIT2)
#define KEYBOARD_BITS_IDCHOOSE (BIT1)

#define IR_REVSTATU_UPDATA (BIT7)
#define IRKEY_BIT_ACTIVE (BIT1)
struct Infrared_Type
{
    u8 statu;
    u8 step;
    u16 count;
    u8 offline;

    struct Receiv_Type
    {
        u32 data;
        u8 bits;
    } dev_rec;
};

void Infrared_Read_Signal(void);
void IR_Reorganize_Datarecd(void);
#endif
