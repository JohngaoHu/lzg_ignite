#include "includes.h"

/***************************************************************************
 * function:        charge process
 * argument in:     none
 * argument out:    none
 * description:     20ms
 * *************************************************************************/
void _bat_process_charging(void)
{
    bat_dev.status |= BATTSTA_BIT_CHARGING; //configure flag to charge
    process.gotosleep_tim = 0;
    bat_dev.shake = 0;

    if (bat_dev.status & BATTSTA_BIT_CHIPSTA) // sta previous level is hight
    {                                         //
        if (!GPIO_Pin_ch_sta)                 // determine whether present lever is low
        {                                     //
            bat_dev.fullsta++;                //
            if (bat_dev.fullsta > 10)         //
            {                                 //
                bat_dev.fullsta = 0;
                bat_dev.status &= ~BATTSTA_BIT_CHIPSTA;
            }
        }
        else
        {
            bat_dev.fullsta = 0;
        }

        if (led_dev.stage != LED_Stage_Turnoff)
        {
            temporary.vars_16b[2] = LED_Stage_Turnoff;
            LED_Write_State();
        }
    }
    else                              // sta previous level is low
    {                                 //
        if (GPIO_Pin_ch_sta)          // determine whether present level is high
        {                             //
            bat_dev.fullsta++;        //
            if (bat_dev.fullsta > 10) //
            {
                bat_dev.fullsta = 0;
                bat_dev.status |= BATTSTA_BIT_CHIPSTA;
            }
        }
        else
        {
            bat_dev.fullsta = 0;
        }

        if (led_dev.stage != LED_Stage_GlintSlow)
        {
            temporary.vars_16b[2] = LED_Stage_GlintSlow;
            LED_Write_State();
        }
    }
}

/***************************************************************************
 * function:        discharge process
 * argument in:     none
 * argument out:    none
 * description:     20ms
 * *************************************************************************/
#define BAT_LOW_DEBOUNCE 25
void _bat_process_discharging(void)
{
    bat_dev.status &= ~BATTSTA_BIT_CHARGING; //configure flag to uncharge

    switch (bat_dev.discharge_step)
    {
    case 0:
        if ((adc_dev.adc_bat.average < BATADC_RESOLUTION8B_3V10) || (adc_dev.adc_vcc.average > VDDADC_RESOLUTION8B_2V90))
        {
            bat_dev.shake++;
            if (bat_dev.shake > BAT_LOW_DEBOUNCE)
            {
                bat_dev.shake = 0;
                bat_dev.discharge_step++;
            }
        }
        else
        {
            bat_dev.shake = 0;
        }

        if (!(motor_dev.status & MOTSTATUS_BITS_ERRSTALL))
        {
            if (led_dev.stage != LED_Stage_Tunron)
            {
                temporary.vars_16b[2] = LED_Stage_Tunron;
                LED_Write_State();
            }
        }
        break;

    case 1:
        bat_dev.status |= BATTSTA_BIT_ALARM;

        if (adc_dev.adc_vcc.average > VDDADC_RESOLUTION8B_2V90)
        {
            bat_dev.shake++;
            if (bat_dev.shake > BAT_LOW_DEBOUNCE)
            {
                bat_dev.shake = 0;
                bat_dev.discharge_step++;
            }
        }
        else
        {
            bat_dev.shake = 0;
            if (adc_dev.adc_bat.average > BATADC_RESOLUTION8B_3V30)
            {
                bat_dev.recover++;
                if (bat_dev.recover > BAT_LOW_DEBOUNCE)
                {
                    bat_dev.recover = 0;
                    bat_dev.discharge_step = 0;
                }
            }
            else
            {
                bat_dev.recover = 0;
            }
        }

        if (led_dev.stage != LED_Stage_GlintSlow)
        {
            temporary.vars_16b[2] = LED_Stage_GlintSlow;
            LED_Write_State();
        }
        break;

    case 2:
        bat_dev.status |= BATTSTA_BIT_INSUFFICIENT;

        if (led_dev.stage != LED_Stage_GlintFast)
        {
            temporary.vars_16b[2] = LED_Stage_GlintFast;
            LED_Write_State();
        }
        break;
    }
}
/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     20ms
 * *************************************************************************/
void Bat_Task_Driver(void)
{
    if (bat_dev.period > 0)
    {
        return;
    }
    bat_dev.period = TICK_MS * 20;

#ifndef OPTIONAL_BMS_EN
    return;
#endif

    switch (bat_dev.stage)
    {
    case 0:                   // initialize
        bat_dev.detect = 100; // config middle value
        bat_dev.stage++;      // jump to next step
        break;

    case 1:                                // detect
        if (adc_dev.adc_usb.average > 125) // usb connected
        {                                  //
            bat_dev.detect++;              // increase
        }                                  //
        else                               // usb not connected
        {                                  //
            bat_dev.detect--;              // decrease
        }                                  //

        if (bat_dev.detect > 110)                       // calculate charge condition
        {                                               //
            bat_dev.stage = 100;                        //
            bat_dev.detect = 0;                         //
            if (!GPIO_Pin_ch_sta)                       //
                bat_dev.status &= ~BATTSTA_BIT_CHIPSTA; //
            else                                        //
                bat_dev.status |= BATTSTA_BIT_CHIPSTA;  //
        }

        if (bat_dev.detect < 90) // calculate discharge condition
        {                        //
            bat_dev.stage = 200; //
            bat_dev.detect = 0;  //
        }
        break;

    case 100: // charging stage
        _bat_process_charging();
        bat_dev.discharge_step = 0;

        if (adc_dev.adc_usb.average < 50)
        {
            bat_dev.detect++;
        }
        else
        {
            bat_dev.detect = 0;
        }

        if (bat_dev.detect > 10)
        {
            bat_dev.stage = 200;
            bat_dev.detect = 0;
        }
        break;

    case 200: // discharge stage
        _bat_process_discharging();

        if (adc_dev.adc_usb.average > 125)
        {
            bat_dev.detect++;
        }
        else
        {
            bat_dev.detect = 0;
        }

        if (bat_dev.detect > 10)
        {
            bat_dev.stage = 100;
            bat_dev.detect = 0;
        }
        break;
    }
}
