#include "includes.h"

#define IR_HANDLEN_4MS 32
#define IR_HANDLEN_2MS 16
#define IR_SIGNAL_BIT1 10

#define DATA_BITS_SIZE 16

#if DATA_BITS_SIZE == 32
#define DATA_HIGHT_BIT (1 << 31)
#endif

#if DATA_BITS_SIZE == 16
#define DATA_HIGHT_BIT (1 << 15)
#endif

#if DATA_BITS_SIZE == 8
#define DATA_HIGHT_BIT (1 << 7)
#endif

/***************************************************************************
 * function:        IR Receiving Sensor
 * argument in:     none
 * argument out:    none
 * description:     it located in the interrupt function, becasue its execution time is short
 * *************************************************************************/
void Infrared_Read_Signal(void)
{
    if (dev_infrared.statu & IR_REVSTATU_UPDATA)
    {
        dev_infrared.step = 0xff;
    }
    

    switch (dev_infrared.step)
    {
    case 0://start
        if (!PIN_INFRARED_REC)
        {
            dev_infrared.step++;
            dev_infrared.count = 0;
        }
        break;

    case 1://start low measure
        if (!PIN_INFRARED_REC)
        {
            dev_infrared.count++;
        }
        else
        {
            if (dev_infrared.count > IR_HANDLEN_4MS)
            {
                dev_infrared.step++;
            }
            else
            {
                dev_infrared.step = 0;
            }
            dev_infrared.count = 0;
        }
        break;

    case 2://start high measure
        if (PIN_INFRARED_REC)
        {
            dev_infrared.count++;
        }
        else
        {
            if (dev_infrared.count > IR_HANDLEN_2MS)
            {
                dev_infrared.step++;
                dev_infrared.dev_rec.bits = 0;
                dev_infrared.dev_rec.data = 0;
            }
            else
            {
                dev_infrared.step = 0;
            }
            dev_infrared.count = 0;
        }
        break;

    case 3:// wait high pulse
        if (PIN_INFRARED_REC)
        {
            dev_infrared.step++;
            dev_infrared.count = 0;
        }
        break;

    case 4:// measure pulse width
        if (PIN_INFRARED_REC)                                // high level
        {                                                    //
            dev_infrared.count++;                            // count increase
            if (dev_infrared.count > IR_HANDLEN_4MS)         // incorrect frame
            {                                                //
                dev_infrared.step = 0xff;                    // jump excute error
            }                                                //
        }                                                    //
        else                                                 // measure pulse width
        {                                                    //
            dev_infrared.dev_rec.data >>= 1;                 // move right
            if (dev_infrared.count > IR_SIGNAL_BIT1)         // bit1
            {                                                //
                dev_infrared.dev_rec.data |= DATA_HIGHT_BIT; // LSB first receive bit0
            }                                                //
            dev_infrared.count = 0;                          //
                                                             //
            dev_infrared.dev_rec.bits++;                     //
            if (dev_infrared.dev_rec.bits == DATA_BITS_SIZE) // frame end
            {                                                //
                dev_infrared.step = 88;                      //
            }                                                //
            else                                             // frame receiving
            {                                                //
                dev_infrared.step -= 1;                      //
            }                                                //
        }                                                    //
        break;

    case 88:
        dev_infrared.statu |= IR_REVSTATU_UPDATA;
        dev_infrared.step = 0;
        dev_infrared.count = 0;
        dev_infrared.dev_rec.bits = 0;
        break;

    case 0xff:
        dev_infrared.step = 0;
        dev_infrared.count = 0;
        dev_infrared.dev_rec.bits = 0;
        break;
    }
}
