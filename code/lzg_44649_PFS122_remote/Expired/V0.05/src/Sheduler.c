#include "includes.h"

/***************************************************************************
 * function:        call back
 * argument in:     none
 * argument out:    none
 * description:     maximum 8 tasks
 * *************************************************************************/
void Callback_event_task(void)
{
    if (led_dev.period)
    {
        led_dev.period--;
    }

    if (motor_dev.period)
    {
        motor_dev.period--;
    }

    if (modescan_dev.period)
    {
        modescan_dev.period--;
    }

    if (bat_dev.period)
    {
        bat_dev.period--;
    }

    if (adc_dev.period)
    {
        adc_dev.period--;
    }
}

/***************************************************************************
 * function:        task process
 * argument in:     none
 * argument out:    none
 * description:
 * *************************************************************************/
void Task_Process_Global(void)
{
    LED_Task_Display();
    Motor_Task_device();
    Backoffice_Task_Manage();
    Bat_Task_Driver();
    ADC_Convert_Loop();
}
