#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

typedef bit bool;
typedef byte uint8_t;
typedef word uint16_t;

typedef byte u8;
typedef word u16;
typedef eword u24;
typedef dword u32;


bit	GPIO_Pin_pwrctr:  PA.0
bit	GPIO_Pin_id_choose:  PA.3
bit	GPIO_Pin_batdetect:  PA.4
bit	GPIO_Pin_ir_receive:  PA.5
bit	GPIO_Pin_modein:  PA.6
bit	GPIO_Pin_ch_sta:  PA.7

bit	GPIO_Pin_Motlcur:  PB.0
bit	GPIO_Pin_Motrcur:  PB.1
bit	GPIO_Pin_motRpwmforward:  PB.2
bit	GPIO_Pin_led:  PB.3
bit	GPIO_Pin_motRpwmreverse:  PB.4
bit	GPIO_Pin_motLpwmreverse:  PB.5
bit	GPIO_Pin_motLpwmforward:  PB.6
bit	GPIO_Pin_USB_IN:  PB.7

#define BIT0 (1 << 0)
#define BIT1 (1 << 1)
#define BIT2 (1 << 2)
#define BIT3 (1 << 3)

#define BIT4 (1 << 4)
#define BIT5 (1 << 5)
#define BIT6 (1 << 6)
#define BIT7 (1 << 7)

#define BIT8 (1 << 8)
#define BIT9 (1 << 9)
#define BIT10 (1 << 10)
#define BIT11 (1 << 11)

#define BIT12 (1 << 12)
#define BIT13 (1 << 13)
#define BIT14 (1 << 14)
#define BIT15 (1 << 15)

#define BIT16 (1 << 16)
#define BIT17 (1 << 17)
#define BIT18 (1 << 18)
#define BIT19 (1 << 19)

#define BIT20 (1 << 20)
#define BIT21 (1 << 21)
#define BIT22 (1 << 22)
#define BIT23 (1 << 23)

#define BIT24 (1 << 24)
#define BIT25 (1 << 25)
#define BIT26 (1 << 26)
#define BIT27 (1 << 27)

#define BIT28 (1 << 28)
#define BIT29 (1 << 29)
#define BIT30 (1 << 30)
#define BIT31 (1 << 31)

#define PIN_INFRARED_REC    GPIO_Pin_ir_receive
#define PINS_NAME_IDCHOOSE GPIO_Pin_id_choose

#define GLOBAL_INTERRUPT_ENABLE engint
#define GLOBAL_INTERRUPT_DISABLE disgint
#define GPIO_INPUT_PATTERN    1
#define GPIO_OUTPUT_PATTERN   0
//#define NULL  0  

//---------------interrupt peroid-----------------------------
#define TICK_MS   20

//----------------User Options---------------------------------
#define  OPTIONAL_SLEEP_EN  //  turn on low power mode
#define  OPTIONAL_BMS_EN    //  turn on battery manager system
#define  MOTOR_PROTECTED    //  protect motor 
#define  REMODE_ID_VERIFY (1) // 
//#define  DEBUG_LOG_EN       //  turn on debug function
#endif

