/*
***************************************************************************
Motor_Left_Forward:     PB5
Motor_Left_Reverse:     PB6

Motor_Right_Forward:    PB4
Motor_Right_Reverse:    PB2

Timer3 output can be selectively output to PB5, PB6, PB7 ,used for Motor L
Timer2 output can be selectively output to PB2, PA3, PB4 ,used for Motor R

motor pmw frequency  98HZ
***************************************************************************
*/

#include "includes.h"

#define Tim_Scalar_default 0x49  //8bit mode, prescalar /16,  clock div /9
#define MotR_Config_Reverse 0x1e //clk source: clk  pb4,pwm mode, no inverse polarity
#define MotR_Config_Forward 0x16 //clk source: clk  pb2,pwm mode, no inverse polarity

#define MotL_Config_Reverse 0x16 //clk source: clk  pb5,pwm mode, no inverse polarity
#define MotL_Config_Forward 0x1a //clk source: clk  pb6,pwm mode, no inverse polarity

/***************************************************************************
 * function:        
 * argument in:     
 * argument out:    
 * description:     
 *                  left motor -timer3  PB5, PB6
 *                  right motor - timer2  PB4, PB2
 * *************************************************************************/
void Motor_Operate_Function(void)
{
    switch (motor_dev.dri_left.direction)
    {
    case MOTOR_CMD_STOP:
        tm2ct = 0x00;
        tm2b = 0X00;
        tm2s = 0X00;
        tm2c = 0X00;
        MotorIO_Right_Fshut();
        MotorIO_Right_Rshut();
        break;

    case MOTOR_CMD_FOREWARD:
#ifdef MOTOR_PROTECTED
        if (motor_dev.dri_left.previous_dir == MOTOR_CMD_REVERSE)
        {
            tm2ct = 0x00;
            tm2b = 0X00;
            tm2s = 0X00;
            tm2c = 0X00;
            MotorIO_Right_Fshut();
            MotorIO_Right_Rshut();
        }
        else
#endif
        {
            tm2ct = 0x00;
            tm2b = motor_dev.dri_left.pwm;
            tm2s = Tim_Scalar_default;
            tm2c = MotR_Config_Forward;
            MotorIO_Right_Rshut();
        }
        break;

    case MOTOR_CMD_REVERSE:
#ifdef MOTOR_PROTECTED
        if (motor_dev.dri_left.previous_dir == MOTOR_CMD_FOREWARD)
        {
            tm2ct = 0x00;
            tm2b = 0X00;
            tm2s = 0X00;
            tm2c = 0X00;
            MotorIO_Right_Fshut();
            MotorIO_Right_Rshut();
        }
        else
#endif
        {
            tm2ct = 0x00;
            tm2b = motor_dev.dri_left.pwm;
            tm2s = Tim_Scalar_default;
            tm2c = MotR_Config_Reverse;
            MotorIO_Right_Fshut();
        }
        break;
    }

    switch (motor_dev.dri_right.direction)
    {
    case MOTOR_CMD_STOP:
        tm3ct = 0x00;
        tm3b = 0X00;
        tm3s = 0X00;
        tm3c = 0X00;
        MotorIO_Left_Fshut();
        MotorIO_Left_Rshut();
        break;

    case MOTOR_CMD_FOREWARD:
#ifdef MOTOR_PROTECTED
        if (motor_dev.dri_right.previous_dir == MOTOR_CMD_REVERSE)
        {
            tm3ct = 0x00;
            tm3b = 0X00;
            tm3s = 0X00;
            tm3c = 0X00;
            MotorIO_Left_Fshut();
            MotorIO_Left_Rshut();
        }
        else
#endif
        {
            tm3ct = 0x00;
            tm3b = motor_dev.dri_right.pwm;
            tm3s = Tim_Scalar_default;
            tm3c = MotL_Config_Forward;
            MotorIO_Left_Rshut();
        }
        break;

    case MOTOR_CMD_REVERSE:
#ifdef MOTOR_PROTECTED
        if (motor_dev.dri_right.previous_dir == MOTOR_CMD_FOREWARD)
        {
            tm3ct = 0x00;
            tm3b = 0X00;
            tm3s = 0X00;
            tm3c = 0X00;
            MotorIO_Left_Fshut();
            MotorIO_Left_Rshut();
        }
        else
#endif
        {
            tm3ct = 0x00;
            tm3b = motor_dev.dri_right.pwm;
            tm3s = Tim_Scalar_default;
            tm3c = MotL_Config_Reverse;
            MotorIO_Left_Fshut();
        }
        break;
    }

#ifdef MOTOR_PROTECTED
    motor_dev.dri_left.previous_dir = motor_dev.dri_left.direction;
    motor_dev.dri_right.previous_dir = motor_dev.dri_right.direction;
#endif
}

/***************************************************************************
 * function:        shut off double motor
 * argument in:     
 * argument out:    none
 * description:     
 * *************************************************************************/
void Motor_Close_Double(void)
{
    motor_dev.dri_left.direction = MOTOR_CMD_STOP;
    motor_dev.dri_left.pwm = 0;
    motor_dev.dri_right.direction = MOTOR_CMD_STOP;
    motor_dev.dri_right.pwm = 0;
    Motor_Operate_Function();
}


