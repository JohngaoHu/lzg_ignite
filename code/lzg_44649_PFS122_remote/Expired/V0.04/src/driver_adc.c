#include "includes.h"

#define ADC_CONVERT_MOTL 0b1_0_0000_00 //PB0-AD0
#define ADC_CONVERT_MOTR 0b1_0_0001_00 //PB1-AD1
#define ADC_CONVERT_BATT 0b1_0_1001_00 //PA4-AD9
#define ADC_CONVERT_USBC 0b1_0_0111_00 //PB7-AD7
#define ADC_CONVERT_BAND 0b1_0_1111_00 //1.2v基准

/***************************************************************************
 * function:        callback
 * argument in:     none
 * argument out:    none
 * description:     called by ad irp
 *                  bat   8bit resolution
 *                  motor current 12bit resolution
 * *************************************************************************/
void adc_callback_irq(void)
{
    WORD data;
    switch (adc_dev.busy)
    {
    case BIT_ADC_BAND:
        adc_dev.busy = 0;
        adc_dev.adc_vcc.read = adcrh;
        adc_dev.adcrl2 = adcrl;
        break;

    case BIT_ADC_BATT:
        adc_dev.busy = 0;
        adc_dev.adc_bat.read = adcrh; //8 bit model
        adc_dev.adcrl2 = adcrl;
        break;

    case BIT_ADC_MOTL:
        adc_dev.busy = 0;
        data$1 = adcrh;
        data$0 = adcrl;
        data = data >> 4;
        adc_dev.adc_motl.read = data;
        break;

    case BIT_ADC_MOTR:
        adc_dev.busy = 0;
        data$1 = adcrh;
        data$0 = adcrl;
        data = data >> 4;
        adc_dev.adc_motr.read = data;
        break;

    case BIT_ADC_USBC:
        adc_dev.busy = 0;
        adc_dev.adc_usb.read = adcrh; //8 bit model
        adc_dev.adcrl2 = adcrl;
        break;

    default:
        adc_dev.busy = 0;
        adc_dev.adcrl2 = adcrh;
        adc_dev.adcrl2 = adcrl;
        break;
    }
}

/***************************************************************************
 * function:        ADC循环检测
 * argument in:     none
 * argument out:    none
 * description:     将外部函数写进来，可以节省少许rom
 * *************************************************************************/
void ADC_Convert_Loop(void)
{
    adc_dev.period = 0;

    switch (adc_dev.step)
    {
    case 0:
        adc_dev.busy = BIT_ADC_BAND;
        adcc = ADC_CONVERT_BAND;
        AD_Start = 1;
        adc_dev.step++;
        break;

    case 1:
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_vcc.sum += adc_dev.adc_vcc.read;
            adc_dev.adc_vcc.average = adc_dev.adc_vcc.sum >> 5;
            adc_dev.adc_vcc.sum -= adc_dev.adc_vcc.average;
        }
        break;

    case 2:
        adc_dev.busy = BIT_ADC_BATT;
        adcc = ADC_CONVERT_BATT;
        AD_Start = 1;
        adc_dev.step++;
        break;

    case 3:
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_bat.sum += adc_dev.adc_bat.read;
            adc_dev.adc_bat.average = adc_dev.adc_bat.sum >> 5;
            adc_dev.adc_bat.sum -= adc_dev.adc_bat.average;
        }
        break;

    case 4:
        adc_dev.busy = BIT_ADC_MOTL;
        adcc = ADC_CONVERT_MOTL;
        AD_Start = 1;
        adc_dev.step++;
        break;

    case 5:
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_motl.sum += adc_dev.adc_motl.read;
            adc_dev.adc_motl.average = adc_dev.adc_motl.sum >> 5;
            adc_dev.adc_motl.sum -= adc_dev.adc_motl.average;
        }
        break;

    case 6:
        adc_dev.busy = BIT_ADC_MOTR;
        adcc = ADC_CONVERT_MOTR;
        AD_Start = 1;
        adc_dev.step++;
        break;

    case 7:
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_motr.sum += adc_dev.adc_motr.read;
            adc_dev.adc_motr.average = adc_dev.adc_motr.sum >> 5;
            adc_dev.adc_motr.sum -= adc_dev.adc_motr.average;
        }
        break;

    case 8:
        adc_dev.busy = BIT_ADC_USBC;
        adcc = ADC_CONVERT_USBC;
        AD_Start = 1;
        adc_dev.step++;
        break;

    case 9:
        if (adc_dev.busy == 0)
        {
            adc_dev.step = 0;
            adc_dev.adc_usb.sum += adc_dev.adc_usb.read;
            adc_dev.adc_usb.average = adc_dev.adc_usb.sum >> 5;
            adc_dev.adc_usb.sum -= adc_dev.adc_usb.average;
        }
        break;
    }
}
