#include "includes.h"

#define ADC_Convert_MotL 0b1_0_0000_00 //PB0
#define ADC_Convert_MotR 0b1_0_0001_00 //PB1
#define ADC_Convert_Batt 0b1_0_1001_00 //PA4
#define ADC_Convert_Band 0b1_0_1111_00 //1.2v基准

/***************************************************************************
 * function:        callback
 * argument in:     none
 * argument out:    none
 * description:     called by ad irp
 *                  bat   8bit resolution
 *                  motor current 12bit resolution
 * *************************************************************************/
void adc_callback_irq(void)
{
    WORD data;
    switch (adc_dev.busy)
    {
#ifdef ADC_CONVERTION_VCC
    case BIT_ADC_GAP:
        adc_dev.busy = 0;
        adc_dev.adc_vcc.read = adcrh;
        adc_dev.adcrl2 = adcrl;
        break;
#endif

#ifdef ADC_Conversion_BAT
    case BIT_ADC_BAT:
        adc_dev.busy = 0;

        adc_dev.adc_bat.read = adcrh;       //8 bit model
        adc_dev.adcrl2 = adcrl;
        
        /*
        data$1 = adcrh; //12bit model
        data$0 = adcrl;
        data   = data >> 4;
        adc_dev.adc_bat.read = data;
        */
        break;
#endif

#ifdef ADC_Conversion_MOTL
    case BIT_ADC_MOTL:
        adc_dev.busy = 0;
        data$1 = adcrh;
        data$0 = adcrl;
        data   = data >> 4;
        adc_dev.adc_motl.read = data;
        break;
#endif

#ifdef ADC_Conversion_MOTR
    case BIT_ADC_MOTR:
        adc_dev.busy = 0;
        data$1 = adcrh;
        data$0 = adcrl;
        data   = data >> 4;
        adc_dev.adc_motr.read = data;
        break;
#endif

    default:
        adc_dev.busy = 0;
        adc_dev.adcrl2 = adcrh;
        adc_dev.adcrl2 = adcrl;
        break;
    }
}

/***************************************************************************
 * function:        ADC循环检测
 * argument in:     none
 * argument out:    none
 * description:     将外部函数写进来，可以节省少许rom
 * *************************************************************************/
void ADC_Convert_Loop(void)
{
    adc_dev.period = 0;
    
    switch (adc_dev.step)
    {
    case 0:
#ifdef ADC_CONVERTION_VCC
        adc_dev.busy = BIT_ADC_GAP;
        adcc = ADC_Convert_Band;
        AD_Start = 1;
#endif
        adc_dev.step++;
        break;

    case 1:
#ifdef ADC_CONVERTION_VCC
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_vcc.sum += adc_dev.adc_vcc.read;
            adc_dev.adc_vcc.average = adc_dev.adc_vcc.sum >> 5;
            adc_dev.adc_vcc.sum -= adc_dev.adc_vcc.average;
        }
#else
        adc_dev.step++;
#endif
        break;

    case 2:
#ifdef ADC_Conversion_BAT
        adc_dev.busy = BIT_ADC_BAT;
        adcc = ADC_Convert_Batt;
        AD_Start = 1;
#endif
        adc_dev.step++;
        break;

    case 3:
#ifdef ADC_Conversion_BAT
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_bat.sum += adc_dev.adc_bat.read;
            adc_dev.adc_bat.average = adc_dev.adc_bat.sum >> 5;
            adc_dev.adc_bat.sum -= adc_dev.adc_bat.average;
        }
#else
        adc_dev.step++;
#endif
        break;

    case 4:
#ifdef ADC_Conversion_MOTL
        adc_dev.busy = BIT_ADC_MOTL;
        adcc = ADC_Convert_MotL;
        AD_Start = 1;
#endif
        adc_dev.step++;
        break;

    case 5:
#ifdef ADC_Conversion_MOTL
        if (adc_dev.busy == 0)
        {
            adc_dev.step++;
            adc_dev.adc_motl.sum += adc_dev.adc_motl.read;
            adc_dev.adc_motl.average = adc_dev.adc_motl.sum >> 5;
            adc_dev.adc_motl.sum -= adc_dev.adc_motl.average;
        }
#else
        adc_dev.step++;
#endif
        break;

    case 6:
#ifdef ADC_Conversion_MOTR
        adc_dev.busy = BIT_ADC_MOTR;
        adcc = ADC_Convert_MotR;
        AD_Start = 1;
#endif
        adc_dev.step++;
        break;

    default:
#ifdef ADC_Conversion_MOTR
        if (adc_dev.busy == 0)
        {
            adc_dev.step = 0;
            adc_dev.adc_motr.sum += adc_dev.adc_motr.read;
            adc_dev.adc_motr.average = adc_dev.adc_motr.sum >> 5;
            adc_dev.adc_motr.sum -= adc_dev.adc_motr.average;
        }
#else
        adc_dev.step = 0;
#endif
        break;
    }
}
