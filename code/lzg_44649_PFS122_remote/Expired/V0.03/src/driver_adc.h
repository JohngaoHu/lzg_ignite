#ifndef _DRIVER_ADC_H_
#define _DRIVER_ADC_H_

#include "typedef.h"

#define BIT_ADC_GAP 0x01
#define BIT_ADC_BAT 0x02
#define BIT_ADC_MOTL 0x04
#define BIT_ADC_MOTR 0x08

#define ADC_Conversion_BAT
#define ADC_Conversion_MOTL
#define ADC_Conversion_MOTR
struct ADC_CTR
{
    u8 period;
    u8 step;
    u8 busy;
    u8 adcrl2;

    struct ADC_VCC_TYPE
    {
        u8 read;
        u16 sum;
        u8 average;
    } adc_vcc;

    struct ADC_BAT_Type
    {
        u8 read;
        u16 sum;
        u8 average;
    } adc_bat;

    struct ADC_MOTL_Type
    {
        u8 read;
        u16 sum;
        u8 average;
    } adc_motl;

    struct ADC_MOTR_Type
    {
        u8 read;
        u16 sum;
        u8 average;
    } adc_motr;
};

void adc_callback_irq(void);
void ADC_Open_BATchannel(void);
void ADC_Open_MotLchannel(void);
void ADC_Open_MotRchannel(void);
void ADC_Convert_Loop(void);
#endif
