#include "includes.h"

/*********************************************************************
 * 
 * ******************************************************************/

//*********************task amount************************************
#define TASKNUM_MAX 6

//*********************task1 default**********************************
#define Task1_Function_Operate() LED_Task_Display()
#define TASK1_VARIABLES_PERIOD led_dev.period

//*********************task2 default**********************************
#define Task2_Function_Operate() Motor_Task_device()
#define TASK2_VARIABLES_PERIOD motor_dev.period

//*********************task3 default**********************************
#define Task3_Function_Operate() Backoffice_Task_Manage()
#define TASK3_VARIABLES_PERIOD modescan_dev.period

//*********************task4 default**********************************
#define Task4_Function_Operate() Bat_Task_Driver()
#define TASK4_VARIABLES_PERIOD bat_dev.period

//*********************task5 default**********************************
#define Task5_Function_Operate() ADC_Convert_Loop()
#define TASK5_VARIABLES_PERIOD adc_dev.period

//*********************task6 default**********************************
#define Task6_Function_Operate() Key_Scan_Function()
#define TASK6_VARIABLES_PERIOD dev_key.period

//*********************task7 default**********************************
#define Task7_Function_Operate() NULL
#define TASK7_VARIABLES_PERIOD NULL

//*********************task8 default**********************************
#define Task8_Function_Operate() NULL
#define TASK8_VARIABLES_PERIOD NULL

/***************************************************************************
 * function:        call back
 * argument in:     none
 * argument out:    none
 * description:     maximum 8 tasks
 * *************************************************************************/
void Callback_event_task(void)
{
#if TASKNUM_MAX > 0
    if (TASK1_VARIABLES_PERIOD)
        TASK1_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 1
    if (TASK2_VARIABLES_PERIOD)
        TASK2_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 2
    if (TASK3_VARIABLES_PERIOD)
        TASK3_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 3
    if (TASK4_VARIABLES_PERIOD)
        TASK4_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 4
    if (TASK5_VARIABLES_PERIOD)
        TASK5_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 5
    if (TASK6_VARIABLES_PERIOD)
        TASK6_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 6
    if (TASK7_VARIABLES_PERIOD)
        TASK7_VARIABLES_PERIOD--;
#endif

#if TASKNUM_MAX > 7
    if (TASK8_VARIABLES_PERIOD)
        TASK8_VARIABLES_PERIOD--;
#endif
}

/***************************************************************************
 * function:        task process
 * argument in:     none
 * argument out:    none
 * description:
 * *************************************************************************/
void Task_Process_Global(void)
{

#if TASKNUM_MAX > 0
    Task1_Function_Operate();
#endif

#if TASKNUM_MAX > 1
    Task2_Function_Operate();
#endif

#if TASKNUM_MAX > 2
    Task3_Function_Operate();
#endif

#if TASKNUM_MAX > 3
    Task4_Function_Operate();
#endif

#if TASKNUM_MAX > 4
    Task5_Function_Operate();
#endif

#if TASKNUM_MAX > 5
    Task6_Function_Operate();
#endif

#if TASKNUM_MAX > 6
    Task7_Function_Operate();
#endif

#if TASKNUM_MAX > 7
    Task8_Function_Operate();
#endif
}
