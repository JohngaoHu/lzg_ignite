#include "includes.h"

/***************************************************************************
 * function:        write
 * argument in:     temporary.vars_16b[2]
 * argument out:    none
 * description:     
 * *************************************************************************/
void LED_Write_State(void)
{
    led_dev.step = 0;
    led_dev.stage = temporary.vars_16b[2];
    led_dev.cnt = 0;
    led_dev.glintnum = 0;
    
}
/*
void LED_Open_State(void){
    led_dev.step = 0;
    led_dev.stage = LED_Stage_Tunron;
    led_dev.cnt = 0;
    led_dev.glintnum = 0;
}

void LED_Close_State(void){
    led_dev.step = 0;
    led_dev.stage = LED_Stage_Turnoff;
    led_dev.cnt = 0;
    led_dev.glintnum = 0;
}
*/
/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     调用周期 Task0_period_t16 (ms)--10ms
 * *************************************************************************/
void LED_Task_Display(void)
{
    if (led_dev.period > 0)
    {
        return;
    }
    led_dev.period = TICK_MS * 10;

    switch (led_dev.stage)
    {
    case LED_Stage_Turnoff:
        LED_Turn_Off;
        led_dev.step = 0;
        led_dev.cnt = 0;
        led_dev.glintnum = 0;
        break;

    case LED_Stage_Tunron:
        LED_Turn_On;
        led_dev.step = 0;
        led_dev.cnt = 0;
        led_dev.glintnum = 0;
        break;

    case LED_Stage_GlintSlow:
        switch (led_dev.step)
        {
        case 0:
            LED_Turn_On;
            led_dev.cnt++;
            if (led_dev.cnt >= 50)
            {
                led_dev.cnt = 0;
                led_dev.step++;
            }
            break;

        default:
            LED_Turn_Off;
            led_dev.cnt++;
            if (led_dev.cnt >= 50)
            {
                led_dev.cnt = 0;
                led_dev.step = 0;
            }
            break;
        }
        break;

    case LED_Stage_GlintQuick: //双闪5次退出， 开250ms 关250ms为闪灯1次
        process.gotosleep_tim = 0;

        switch (led_dev.step)
        {
        case 0:
            LED_Turn_Off;
            led_dev.cnt++;
            if (led_dev.cnt >= 25)
            {
                led_dev.cnt = 0;
                led_dev.step++;
            }
            break;

        case 1:
        case 3:
            LED_Turn_On;
            led_dev.cnt++;
            if (led_dev.cnt >= 25)
            {
                led_dev.cnt = 0;
                led_dev.step++;
            }
            break;

        case 2:
        case 4:
            LED_Turn_Off;
            led_dev.cnt++;
            if (led_dev.cnt >= 25)
            {
                led_dev.cnt = 0;
                led_dev.step++;
            }
            break;

        default:
            LED_Turn_Off;
            led_dev.cnt++;
            if (led_dev.cnt >= 100)
            {
                led_dev.cnt = 0;
                led_dev.step = 1;
                led_dev.glintnum++;
                if (led_dev.glintnum >= 5)
                {
                    led_dev.glintnum = 0;
                    Motor_Close_Double();
                    led_dev.stage = LED_Stage_Tunron;             //finish glink quick, go to turn on
                    motor_dev.status &= ~MOTSTATUS_BITS_ERRSTALL; //clear motor stall error
                }
            }
            break;
        }
        break;
    }
}
