#ifndef _DEVICE_IR_H_
#define _DEVICE_IR_H_

#include "typedef.h"

#define IRKEY_BIT_ACTIVE     (1 << 1)


#define KEYBOARD_BITS_FORWARDKEY (1 << 5)
#define KEYBOARD_BITS_BACKWARDKEY (1 << 4)
#define KEYBOARD_BITS_STEERINGLEFT (1 << 6)
#define KEYBOARD_BITS_STEERINGRIGHT (1 << 7)
#define KEYBOARD_BITS_TRIMLEFT (1 << 2)
#define KEYBOARD_BITS_TRIMRIGHT (1 << 3)
#define KEYBOARD_BITS_IDCHOOSE (1 << 1)

#define IR_REVSTATU_UPDATA  (1 << 7)
struct Infrared_Type{
    u8 statu;
    u8 step;
    u16 count;
    u8 offline;

    struct Receiv_Type{
        u32 data;
        u8 bits;
    }dev_rec;
};

void Infrared_Read_Signal(void);
void IR_Reorganize_Datarecd(void);
#endif
