#include "includes.h"

/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     20ms
 * *************************************************************************/
void Bat_Task_Driver(void)
{

    if (bat_dev.period > 0)
    {
        return;
    }
    bat_dev.period = TICK_MS * 20;

#ifdef OPTIONAL_BMS_EN
    if (!GPIO_Pin_ch_sta)                       // if io is a low level, it means than it must be charging
    {                                           //
        bat_dev.status |= BATTSTA_BIT_CHARGING; //configure flag to charge
        process.gotosleep_tim = 0;              //clean sleep counter

        if (led_dev.stage != LED_Stage_GlintSlow)
        {
            temporary.vars_16b[2] = LED_Stage_GlintSlow;
            LED_Write_State();
        }

        bat_dev.status |= BATTSTA_BIT_STEP;
        bat_dev.shake = 0;
    }
    else // if io is a high level, it could be in one of two stages, charge completion or no charge
    {
        if ((led_dev.stage == LED_Stage_Turnoff) ||
            (led_dev.stage == LED_Stage_GlintSlow))
        {                                             //whether charge completion or no charge, led can not off or glint
            temporary.vars_16b[2] = LED_Stage_Tunron; //set stage to continual on
            LED_Write_State();                        //
        }

        if (bat_dev.status & BATTSTA_BIT_STEP)                        //jump here from charging stage
        {                                                             //
            if (adc_dev.adc_bat.average > BATADC_RESOLUTION8B_USBCON) //it means usb cable is connected
            {
                bat_dev.status |= BATTSTA_BIT_CHARGING;
                //process.gotosleep_tim = 0;
                bat_dev.shake = 0;
            }
            else
            {
                bat_dev.shake++;
                if (bat_dev.shake > 100)
                {
                    bat_dev.shake = 0;
                    bat_dev.status &= ~BATTSTA_BIT_STEP;
                }
            }
        }
        else
        {
            bat_dev.status &= ~BATTSTA_BIT_CHARGING;                    //configure flag to uncharge
            if ((adc_dev.adc_vcc.average > VDDADC_RESOLUTION8B_3V0) ||  //用内置1.2V，反推VDD，值越大，VDD越低
                (adc_dev.adc_bat.average < BATADC_RESOLUTION8B_TOOLOW)) //正常112， 低电压为120
            {
                bat_dev.shake++;
                if (bat_dev.shake > 100)
                {
                    bat_dev.shake = 0;
                    bat_dev.status |= BATTSTA_BIT_INSUFFICIENT;
                }
            }
            else
            {
                bat_dev.shake = 0;
            }
        }
    }
#endif
}
