#ifndef _DRIVER_PWM_H_
#define _DRIVER_PWM_H_

#define MotorIO_Left_Fshut()  GPIO_Pin_motLpwmforward = 0
#define MotorIO_Left_Rshut()  GPIO_Pin_motLpwmreverse = 0
#define MotorIO_Right_Fshut() GPIO_Pin_motRpwmforward = 0
#define MotorIO_Right_Rshut() GPIO_Pin_motRpwmreverse = 0

void Motor_Close_Double(void);
void Motor_Operate_Function(void);
#endif
