#ifndef _DRIVER_BAT_H_
#define _DRIVER_BAT_H_
#include "typedef.h"

//--------8bit  ref3v  ����1/2--------------------
#define BATADC_RESOLUTION8B_TOOLOW 127
#define BATADC_RESOLUTION8B_USBCON 195

#define VDDADC_RESOLUTION8B_3V0    115


#define BATTSTA_BIT_STEP            (1 << 3)    //temporary variables
#define BATTSTA_BIT_CHARGING        (1 << 2)    //charging stage
#define BATTSTA_BIT_CHARGEFULL      (1 << 1)    //charge completed
#define BATTSTA_BIT_INSUFFICIENT    (1 << 0)    //power defected

struct Bat_CTR
{
    u16 period;
    u8 status;
    u8 shake;
};

void Bat_Task_Driver(void);
#endif
