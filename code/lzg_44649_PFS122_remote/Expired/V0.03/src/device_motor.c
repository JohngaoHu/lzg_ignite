#include "includes.h"

/***************************************************************************
 * function     :   calculate dynamic value with compensation parameter      
 * argument in  :   temporary.vars_8b[1]  origin dynamic
 * argument out :   
 * description  :   
 *                  only be used straight running
 *                  if compensation on right of middle location, it means left motor decrease dynamic
 *                  if compensation on left of middle location , it menas right motor need to decrease dynamic
 * *************************************************************************/
void Motor_Process_Compensation(void)
{
    if (motor_dev.compensation == CALIBRATION_CENTRE_DEFAULT)
    {
        motor_dev.dri_left.pwm = temporary.vars_8b[1];
        motor_dev.dri_right.pwm = temporary.vars_8b[1];
    }
    else
    {
        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.vars_8b[2] = motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT;
            motor_dev.dri_right.pwm = temporary.vars_8b[1];
            if (temporary.vars_8b[1] > temporary.vars_8b[2])
            {
                temporary.vars_8b[1] = temporary.vars_8b[1] - temporary.vars_8b[2];
            }
            else
            {
                temporary.vars_8b[1] = 0;
            }
            motor_dev.dri_left.pwm = temporary.vars_8b[1];
        }
        else
        {
            temporary.vars_8b[2] = CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation;
            motor_dev.dri_left.pwm = temporary.vars_8b[1];
            if (temporary.vars_8b[1] > temporary.vars_8b[2])
            {
                temporary.vars_8b[1] = temporary.vars_8b[1] - temporary.vars_8b[2];
            }
            else
            {
                temporary.vars_8b[1] = 0;
            }
            motor_dev.dri_right.pwm = temporary.vars_8b[1];
        }
    }
}

/***************************************************************************
 * function     :   Write control parameter to dynamical system        
 * argument in  :   
 *                  temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 * argument out :    
 * description  :     
 * *************************************************************************/
void Motor_Algorithm_Running(void)
{
    if (motor_dev.compensation == 0)
    {
        motor_dev.compensation = CALIBRATION_CENTRE_DEFAULT;
    }
    motor_dev.dri_left.previous_dir = motor_dev.dri_left.direction;
    motor_dev.dri_right.previous_dir = motor_dev.dri_right.direction;

    switch (temporary.vars_8b[0])
    {
    case DYNAMIC_DIR_STOP:
        motor_dev.dri_left.direction = MOTOR_CMD_STOP;
        motor_dev.dri_right.direction = MOTOR_CMD_STOP;
        motor_dev.dri_left.pwm = 0;
        motor_dev.dri_right.pwm = 0;
        break;

    case DYNAMIC_DIR_FORWARD:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        Motor_Process_Compensation();
        break;

    case DYNAMIC_DIR_REVERSE:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        Motor_Process_Compensation();
        break;

    case DYNAMIC_DIR_LEFTUP:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        Motor_Process_Compensation();

        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.vars_16b[0] = motor_dev.dri_left.pwm + ((motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT) >> 1);
            motor_dev.dri_left.pwm = temporary.vars_16b[0] >> 2;
        }
        else
        {
            motor_dev.dri_left.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_RIGHTUP:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        Motor_Process_Compensation();

        if (motor_dev.compensation < CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.vars_16b[0] = motor_dev.dri_right.pwm + ((CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation) >> 1);
            motor_dev.dri_right.pwm >>= 2;
        }
        else
        {
            motor_dev.dri_right.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_LEFTDN:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        Motor_Process_Compensation();

        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.vars_16b[0] = motor_dev.dri_left.pwm + ((motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT) >> 1);
            motor_dev.dri_left.pwm = temporary.vars_16b[0] >> 2;
        }
        else
        {
            motor_dev.dri_left.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_RIGHTDN:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        Motor_Process_Compensation();

        if (motor_dev.compensation < CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.vars_16b[0] = motor_dev.dri_right.pwm + ((CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation) >> 1);
            motor_dev.dri_right.pwm >>= 2;
        }
        else
        {
            motor_dev.dri_right.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_LEFT:
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.pwm = temporary.vars_8b[1];

        temporary.vars_8b[1] = 0xff - temporary.vars_8b[1];
        if (temporary.vars_8b[1] == 0)
        {
            temporary.vars_8b[1] = 127;
        }
        motor_dev.dri_left.pwm = temporary.vars_8b[1];

        break;

    case DYNAMIC_DIR_RIGHT:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_left.pwm = temporary.vars_8b[1];

        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        temporary.vars_8b[1] = 0xff - temporary.vars_8b[1];
        if (temporary.vars_8b[1] == 0)
        {
            temporary.vars_8b[1] = 127;
        }
        motor_dev.dri_right.pwm = temporary.vars_8b[1];
        break;
    }
}

#if ANALYSIS_RECBYTE_CONFIG == ANALYSIS_RECBYTE_1VERSION
/***************************************************************************
 * function:        Verify receive data
 * argument in:     temporary.vars_8b[0]
 * argument out:    temporary.vars_16b[0]  0 success 1 failure
 * description:     
 * *************************************************************************/
void Motor_Verify_IRrec1byte(void)
{
    temporary.vars_8b[1] = (temporary.vars_8b[0] >> 6) & 0x03;
    temporary.vars_8b[2] = (temporary.vars_8b[0] >> 4) & 0x03;
    temporary.vars_16b[1] = (temporary.vars_8b[1] ^ temporary.vars_8b[2]) & 0x03;

    temporary.vars_8b[1] = (temporary.vars_8b[0] >> 2) & 0x03;
    temporary.vars_8b[2] = temporary.vars_16b[1] & 0x03;
    temporary.vars_16b[1] = (temporary.vars_8b[1] ^ temporary.vars_8b[2]) & 0x03;

    temporary.vars_8b[1] = temporary.vars_8b[0] & 0x03;
    temporary.vars_8b[2] = temporary.vars_16b[1] & 0x03;

    if (temporary.vars_8b[1] == temporary.vars_8b[2])
    {
        temporary.vars_16b[0] = 0;
    }
    else
    {
        temporary.vars_16b[0] = 0xffff;
    }
}

/***************************************************************************
 * function:        single byte analysis version
 * argument in:     none
 * argument out:    temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 * description:     
 * *************************************************************************/
void _analysis_receive_version1Byte(void)
{
    temporary.vars_8b[0] = dev_infrared.dev_rec.data & 0xff;
    Motor_Verify_IRrec1byte();

    if (temporary.vars_16b[0] == 0)
    {
        //fine - right
        if (temporary.vars_8b[0] & KEYBOARD_BITS_TRIMRIGHT)
        {
            if (motor_dev.compensation > (COMPENSATION_LEFT_MIN + COMPENSATION_ADJUST_EXTENT))
            {
                motor_dev.compensation -= COMPENSATION_ADJUST_EXTENT;
            }
            else
            {
                motor_dev.compensation = COMPENSATION_LEFT_MIN;
            }
        }

        //fine - left
        if (temporary.vars_8b[0] & KEYBOARD_BITS_TRIMLEFT)
        {
            if (motor_dev.compensation < (COMPENSATION_RIGHT_MAX - COMPENSATION_ADJUST_EXTENT))
            {
                motor_dev.compensation += COMPENSATION_ADJUST_EXTENT;
            }
            else
            {
                motor_dev.compensation = COMPENSATION_RIGHT_MAX;
            }
        }

        // forward - press
        if (temporary.vars_8b[0] & KEYBOARD_BITS_FORWARDKEY)
        {
            if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT)
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_LEFTUP;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
            else if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGRIGHT)
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTUP;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
            else
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_FORWARD;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
        }
        // reverse - press
        else if (temporary.vars_8b[0] & KEYBOARD_BITS_BACKWARDKEY)
        {
            if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT)
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_LEFTDN;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
            else if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGRIGHT)
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTDN;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
            else
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_REVERSE;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
        }
        // only left or right press
        else if ((temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT) || (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGRIGHT))
        {
            if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT)
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_LEFT;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
            else
            {
                temporary.vars_8b[0] = DYNAMIC_DIR_RIGHT;
                temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;
            }
        }
        // all key float
        else
        {
            temporary.vars_8b[0] = DYNAMIC_DIR_STOP;
            temporary.vars_8b[1] = 0;
        }
    }
}
#endif

#if ANALYSIS_RECBYTE_CONFIG == ANALYSIS_RECBYTE_2VERSION
/***************************************************************************
 * function:        single byte analysis version
 * argument in:     none
 * argument out:    temporary.vars_8b[0]  Dynamical_Dir
 *                  temporary.vars_8b[1]  dynamic_range
 * description:     
 * *************************************************************************/
void _analysis_receive_version2Byte(void)
{
    temporary.vars_16b[0] = dev_infrared.dev_rec.data & 0xffff; // get receive data
                                                                //
    temporary.vars_8b[0] = dev_infrared.dev_rec.data >> 8;      // achieve high 8bit
    temporary.vars_8b[1] = dev_infrared.dev_rec.data;           // achieve low  8bit
    temporary.vars_8b[1] = ~temporary.vars_8b[1];               // negation data
    if (temporary.vars_8b[0] != temporary.vars_8b[1])           // verify data corectness
    {                                                           //
        temporary.vars_8b[0] = DYNAMIC_DIR_STOP;                //
        temporary.vars_8b[1] = 0;                               //
        return;                                                 //
    }                                                           //
                                                                //
                                                                /* if (temporary.vars_8b[0] & KEYBOARD_BITS_IDCHOOSE)          // remote signal : id short to groud
    {                                                           //
        if (dev_key.state & KEYBOARD_BITS_IDCHOOSE)             //
        {                                                       //
            ;                                                   //
        }                                                       //
        else                                                    //
        {                                                       //
            temporary.vars_8b[0] = DYNAMIC_DIR_STOP;            //
            temporary.vars_8b[1] = 0;                           //
            return;                                             //
        }                                                       //
    }                                                           //
    else                                                        // remote signal : id float
    {                                                           //
        if (dev_key.state & KEYBOARD_BITS_IDCHOOSE)             // 
        {                                                       //
            temporary.vars_8b[0] = DYNAMIC_DIR_STOP;            //
            temporary.vars_8b[1] = 0;                           //
            return;                                             //
        }                                                       //
        else                                                    //
        {                                                       //
            ;                                                   //
        }                                                       //
    }
*/

    if (temporary.vars_8b[0] & KEYBOARD_BITS_TRIMRIGHT)                                    // fine turning right
    {                                                                                      //
        if (motor_dev.compensation > (COMPENSATION_LEFT_MIN + COMPENSATION_ADJUST_EXTENT)) //
        {                                                                                  //
            motor_dev.compensation -= COMPENSATION_ADJUST_EXTENT;                          //
        }                                                                                  //
        else                                                                               //
        {                                                                                  //
            motor_dev.compensation = COMPENSATION_LEFT_MIN;                                //
        }                                                                                  //
    }                                                                                      //

    if (temporary.vars_8b[0] & KEYBOARD_BITS_TRIMLEFT)                                      // fine turning left
    {                                                                                       //
        if (motor_dev.compensation < (COMPENSATION_RIGHT_MAX - COMPENSATION_ADJUST_EXTENT)) //
        {                                                                                   //
            motor_dev.compensation += COMPENSATION_ADJUST_EXTENT;                           //
        }                                                                                   //
        else                                                                                //
        {                                                                                   //
            motor_dev.compensation = COMPENSATION_RIGHT_MAX;                                //
        }                                                                                   //
    }                                                                                       //

    if (temporary.vars_8b[0] & KEYBOARD_BITS_FORWARDKEY)             // forward key
    {                                                                //
        if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT)       // + left
        {                                                            //
            temporary.vars_8b[0] = DYNAMIC_DIR_LEFTUP;               //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;               //
        }                                                            //
        else if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGRIGHT) // + right
        {                                                            //
            temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTUP;              //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;               //
        }                                                            //
        else                                                         // only forward
        {                                                            //
            temporary.vars_8b[0] = DYNAMIC_DIR_FORWARD;              //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;               //
        }                                                            //
    }                                                                //

    else if (temporary.vars_8b[0] & KEYBOARD_BITS_BACKWARDKEY)       // backward key
    {                                                                //
        if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT)       // + left
        {                                                            //
            temporary.vars_8b[0] = DYNAMIC_DIR_LEFTDN;               //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;               //
        }                                                            //
        else if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGRIGHT) // + right
        {                                                            //
            temporary.vars_8b[0] = DYNAMIC_DIR_RIGHTDN;              //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;               //
        }                                                            //
        else                                                         // only backward
        {                                                            //
            temporary.vars_8b[0] = DYNAMIC_DIR_REVERSE;              //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;               //
        }                                                            //
    }                                                                //
    // only left or right press
    else if ((temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT) || //    only left
             (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGRIGHT))  //    only right
    {                                                               //
        if (temporary.vars_8b[0] & KEYBOARD_BITS_STEERINGLEFT)      //    left
        {                                                           //
            temporary.vars_8b[0] = DYNAMIC_DIR_LEFT;                //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;              //
        }                                                           //
        else                                                        //    right
        {                                                           //
            temporary.vars_8b[0] = DYNAMIC_DIR_RIGHT;               //
            temporary.vars_8b[1] = MOTOR_SPEED_CONFIG;              //
        }                                                           //
    }                                                               //
                                                                    //
    else                                                            // all key float
    {                                                               //
        temporary.vars_8b[0] = DYNAMIC_DIR_STOP;                    //
        temporary.vars_8b[1] = 0;                                   //
    }                                                               //
}

#endif
/***************************************************************************
 * function:        Analysis receive data
 * argument in:     none
 * argument out:    none
 * description:     50m
 * *************************************************************************/
void Motor_Analysis_IRrec(void)
{
    if (bat_dev.status & BATTSTA_BIT_CHARGING)
    {
        temporary.vars_8b[0] = DYNAMIC_DIR_STOP;
        temporary.vars_8b[1] = 0;
    }
    else
    {
        if (dev_infrared.statu & IR_REVSTATU_UPDATA)
        {
            //LED_Open_State();
            dev_infrared.offline = 0;
            process.gotosleep_tim = 0;

#if ANALYSIS_RECBYTE_CONFIG == ANALYSIS_RECBYTE_1VERSION
            _analysis_receive_version1Byte();
#endif

#if ANALYSIS_RECBYTE_CONFIG == ANALYSIS_RECBYTE_2VERSION
            _analysis_receive_version2Byte();
#endif
            dev_infrared.statu &= ~IR_REVSTATU_UPDATA;
        }
        else
        {
            //LED_Close_State();
            if ((motor_dev.dri_left.pwm > 0) || (motor_dev.dri_right.pwm > 0))
            {
                dev_infrared.offline++;
                if (dev_infrared.offline > 5)
                {
                    dev_infrared.offline = 0;
                    temporary.vars_8b[0] = DYNAMIC_DIR_STOP;
                    temporary.vars_8b[1] = 0;
                }
            }
        }
    }

    Motor_Algorithm_Running();
}

/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     50ms
 * *************************************************************************/
void Motor_Task_device(void)
{
    if (motor_dev.period > 0)
    {
        return;
    }
    motor_dev.period = TICK_MS * 50;
    Motor_Analysis_IRrec();
    Motor_Operate_Function();
}
