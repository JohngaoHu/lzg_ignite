#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#include "../src/typedef.h"
#include "../src/global.h"
#include "../src/extern.h"
#include "../src/sheduler.h"
#include "../src/driver_led.h"
#include "../src/driver_pwm.h"
#include "../src/device_motor.h"
#include "../src/device_ir.h"
#include "../src/device_backoffice.h"
#include "../src/driver_bat.h"
#include "../src/device_key.h"
#include "../src/debug_log.h"

#endif

