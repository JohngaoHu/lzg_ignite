#ifndef _DRIVER_LED_H_
#define _DRIVER_LED_H_

#include "../src/typedef.h"

#define LED_Turn_On GPIO_Pin_led = 0
#define LED_Turn_Off GPIO_Pin_led = 1

#define LED_Stage_Turnoff 0
#define LED_Stage_Tunron 1
#define LED_Stage_GlintQuick 2
#define LED_Stage_GlintSlow 3
struct LED_CTR
{
   u8 period;
   u8 step;
   u8 stage;
   u8 cnt;
   u8 glintnum;
};

void LED_Open_State(void);
void LED_Close_State(void);
void LED_Task_Display(void);
void LED_Write_State(void);
#endif
