#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

typedef bit bool;
typedef byte uint8_t;
typedef word uint16_t;

typedef byte u8;
typedef word u16;
typedef eword u24;
typedef dword u32;

bit	GPIO_Pin_pwrctr:  PA.0
bit	GPIO_Pin_ir_ba_data:  PA.3
bit	GPIO_Pin_batdetect:  PA.4
bit	GPIO_Pin_ir_receive:  PA.5
bit	GPIO_Pin_modein:  PA.6
bit	GPIO_Pin_ch_sta:  PA.7

bit	GPIO_Pin_Motlcur:  PB.0
bit	GPIO_Pin_Motrcur:  PB.1
bit	GPIO_Pin_motRpwmforward:  PB.2
bit	GPIO_Pin_led:  PB.3
bit	GPIO_Pin_motRpwmreverse:  PB.4
bit	GPIO_Pin_motLpwmreverse:  PB.5
bit	GPIO_Pin_motLpwmforward:  PB.6
bit	GPIO_Pin_id_choose:  PB.7

#define PIN_INFRARED_REC    GPIO_Pin_ir_receive
#define PINS_NAME_IDCHOOSE GPIO_Pin_id_choose

#define GLOBAL_INTERRUPT_ENABLE engint
#define GLOBAL_INTERRUPT_DISABLE disgint
#define GPIO_INPUT_PATTERN    1
#define GPIO_OUTPUT_PATTERN   0
//#define NULL  0  

//---------------interrupt peroid-----------------------------
#define TICK_MS   20

//----------------User Options---------------------------------
#define  ADC_CONVERTION_VCC //  measure VDD Voltage
#define  OPTIONAL_SLEEP_EN  //  turn on low power mode
#define  OPTIONAL_BMS_EN    //  turn on battery manager system
#define  MOTOR_PROTECTED    //  protect motor 
//#define  DEBUG_LOG_EN       //  turn on debug function
#endif

