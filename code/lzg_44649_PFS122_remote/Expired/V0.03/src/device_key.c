#include "includes.h"

/*******************************************************************************************************
 * functional	:	git gpio vol level
 * argument in 	:	(temporary.vars_16b[0]) key
 * argument out :	(temporary.vars_16b[1])  
 * description	:	none
 * ****************************************************************************************************/
void Button_Read_lever(void)
{
    switch (temporary.vars_16b[0])
    {
    case BtnName_idchoose:
        if (PINS_NAME_IDCHOOSE == 0)
            temporary.vars_16b[1] = 0;
        else
            temporary.vars_16b[1] = 1;
        break;

    default:

        break;
    }
}

/*******************************************************************************************************
 * functional	:	read key
 * argument in 	:	(temporary.vars_8b[0]) step
 *                  (temporary.vars_8b[1]) shake
 *                  (temporary.vars_8b[2]) key
 * argument out :	(temporary.vars_8b[3]) btn_press or btn_null
 *                  (temporary.vars_8b[0]) step
 *                  (temporary.vars_8b[1]) shake
 * description	:	none
 * ****************************************************************************************************/
void Button_Read_Single(void)
{
    u8 valid = 0;

    temporary.vars_8b[3] = btn_null;

    switch (temporary.vars_8b[0])
    {
    case 0:                                           // detect
        temporary.vars_16b[0] = temporary.vars_8b[2]; // get key number
        Button_Read_lever();                          // read gpio level
        if (temporary.vars_16b[1] == 0)               // if level = 0, valid
        {                                             //
            temporary.vars_8b[1] = 0;                 // clear shake
            temporary.vars_8b[0]++;                   // step increase
        }
        break;

    case 1:                                           // shake
        temporary.vars_16b[0] = temporary.vars_8b[2]; // get key number
        Button_Read_lever();                          // read gpio level
        if (temporary.vars_16b[1] == 0)               // if level = 0, valid
        {                                             //
            temporary.vars_8b[1]++;                   // shake++
            if (temporary.vars_8b[1] > KEY_SHAKE_MAX) // pass
            {                                         //
                temporary.vars_8b[0]++;               // step increase
                temporary.vars_8b[1] = 0;             // clear shake
                temporary.vars_8b[3] = btn_press;     // return valid
            }                                         //
        }                                             //
        else                                          //
        {                                             // incorrect
            temporary.vars_8b[0] = 0;                 // step clear
        }
        break;

    case 2:                                             // wait key release
        temporary.vars_16b[0] = temporary.vars_8b[2];   // get key number
        Button_Read_lever();                            // read gpio level
        if (temporary.vars_16b[1] == 0)                 // if level = 0, valid
        {                                               //
            temporary.vars_8b[1] = 0;                   // clear shake
            temporary.vars_8b[3] = btn_press;           // return valid
        }                                               //
        else                                            //
        {                                               //
            temporary.vars_8b[1]++;                     // shake++
            if (temporary.vars_8b[1] > KEY_RELEASE_MIN) // reach long press threshold
            {                                           //
                temporary.vars_8b[3] = btn_null;        //
                temporary.vars_8b[1] = 0;               // clear shake
                temporary.vars_8b[0] = 0;               // clear step
            }
        }
        break;
    }
}

/*******************************************************************************************************
 * functional	:	user code enter
 * argument in 	:	none
 * argument out :	none
 * description	:	none
 * ****************************************************************************************************/
void Key_Scan_Function(void)
{
    if (dev_key.period > 0)
    {
        return;
    }
    dev_key.period = TICK_MS * 5;

    //**************idchoose****************************
    temporary.vars_8b[0] = dev_key.idchoose.step;
    temporary.vars_8b[1] = dev_key.idchoose.shake;
    temporary.vars_8b[2] = BtnName_idchoose;
    Button_Read_Single();
    dev_key.idchoose.step = temporary.vars_8b[0];
    dev_key.idchoose.shake = temporary.vars_8b[1];
    if (temporary.vars_8b[3] != btn_null)
        dev_key.state |= KEYBOARD_BITS_IDCHOOSE;
    else
        dev_key.state &= ~KEYBOARD_BITS_IDCHOOSE;

}
