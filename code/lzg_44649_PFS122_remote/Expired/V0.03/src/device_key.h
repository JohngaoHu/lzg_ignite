#ifndef _DEVICE_KEY_H_
#define _DEVICE_KEY_H_

#include "../src/typedef.h"


#define KEY_SHAKE_MAX  2
#define KEY_LONG_SHORT 160
#define KEY_RELEASE_MIN 20

enum
{
    btn_null = 0,
    btn_press,
    btn_continuous,
};

enum
{
    BtnName_idchoose = 0,
};

#define KEYBOARD_BITS_IDCHOOSE (1 << 1)

struct Key_CTR
{
    u16 period;
    u8 state;

    struct Idchoose_CTR{
        u8 step;
        u8 shake;
    }idchoose;
};
void Key_Scan_Function(void);
#endif

