#ifndef _DEVICE_BACKOFFICE_H_
#define _DEVICE_BACKOFFICE_H_

#include "typedef.h"

//测试发现，数量最大可达90AD
//空载约17AD左右
#define STALL_THRESHOLD_GOUND 38 //45			//step=0.66mV(25mV)0.1R(250mA)
#define STALL_THRESHOLD_CLIMP 45 //53//45		//step=0.66mV(30mV)0.1R(300mA)

//防抖时间
//5MS计量单位
#define STALL_SHAKE_GOUND     25
#define STALL_SHAKE_CLIMP     50

struct Modescan_CTR
{
    u8 mode; //0: 走地模式  1：爬墙模式
    u8 step;
    u8 shake;
};

void Backoffice_Task_Manage(void);
#endif
