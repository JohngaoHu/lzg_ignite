#include "includes.h"

/***************************************************************************
 * function     :   calculate dynamic value with compensation parameter      
 * argument in  :   temporary.mem8b[1]  origin dynamic
 * argument out :   
 * description  :   
 *                  only be used straight running
 *                  if compensation on right of middle location, it means left motor decrease dynamic
 *                  if compensation on left of middle location , it menas right motor need to decrease dynamic
 * *************************************************************************/
void Motor_Process_Compensation(void)
{
    if (motor_dev.compensation == CALIBRATION_CENTRE_DEFAULT)
    {
        motor_dev.dri_left.pwm = temporary.mem8b[1];
        motor_dev.dri_right.pwm = temporary.mem8b[1];
    }
    else
    {
        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.mem8b[2] = motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT;
            motor_dev.dri_right.pwm = temporary.mem8b[1];
            if (temporary.mem8b[1] > temporary.mem8b[2])
            {
                temporary.mem8b[1] = temporary.mem8b[1] - temporary.mem8b[2];
            }
            else
            {
                temporary.mem8b[1] = 0;
            }
            motor_dev.dri_left.pwm = temporary.mem8b[1];
        }
        else
        {
            temporary.mem8b[2] = CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation;
            motor_dev.dri_left.pwm = temporary.mem8b[1];
            if (temporary.mem8b[1] > temporary.mem8b[2])
            {
                temporary.mem8b[1] = temporary.mem8b[1] - temporary.mem8b[2];
            }
            else
            {
                temporary.mem8b[1] = 0;
            }
            motor_dev.dri_right.pwm = temporary.mem8b[1];
        }
    }
}

/***************************************************************************
 * function     :   Write control parameter to dynamical system        
 * argument in  :   
 *                  temporary.mem8b[0]  Dynamical_Dir
 *                  temporary.mem8b[1]  dynamic_range
 * argument out :    
 * description  :     
 * *************************************************************************/
void Motor_Algorithm_Running(void)
{
    if (motor_dev.compensation == 0)
    {
        motor_dev.compensation = CALIBRATION_CENTRE_DEFAULT;
    }
    motor_dev.dri_left.previous_dir = motor_dev.dri_left.direction;
    motor_dev.dri_right.previous_dir = motor_dev.dri_right.direction;

    switch (temporary.mem8b[0])
    {
    case DYNAMIC_DIR_STOP:
        motor_dev.dri_left.direction = MOTOR_CMD_STOP;
        motor_dev.dri_right.direction = MOTOR_CMD_STOP;
        motor_dev.dri_left.pwm = 0;
        motor_dev.dri_right.pwm = 0;
        break;

    case DYNAMIC_DIR_FORWARD:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        Motor_Process_Compensation();
        break;

    case DYNAMIC_DIR_REVERSE:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        Motor_Process_Compensation();
        break;

    case DYNAMIC_DIR_LEFTUP:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        Motor_Process_Compensation();

        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.mem16b[0] = motor_dev.dri_left.pwm + ((motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT) >> 1);
            motor_dev.dri_left.pwm = temporary.mem16b[0] >> 2;
        }
        else
        {
            motor_dev.dri_left.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_RIGHTUP:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        Motor_Process_Compensation();

        if (motor_dev.compensation < CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.mem16b[0] = motor_dev.dri_right.pwm + ((CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation) >> 1);
            motor_dev.dri_right.pwm >>= 2;
        }
        else
        {
            motor_dev.dri_right.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_LEFTDN:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        Motor_Process_Compensation();

        if (motor_dev.compensation > CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.mem16b[0] = motor_dev.dri_left.pwm + ((motor_dev.compensation - CALIBRATION_CENTRE_DEFAULT) >> 1);
            motor_dev.dri_left.pwm = temporary.mem16b[0] >> 2;
        }
        else
        {
            motor_dev.dri_left.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_RIGHTDN:
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        Motor_Process_Compensation();

        if (motor_dev.compensation < CALIBRATION_CENTRE_DEFAULT)
        {
            temporary.mem16b[0] = motor_dev.dri_right.pwm + ((CALIBRATION_CENTRE_DEFAULT - motor_dev.compensation) >> 1);
            motor_dev.dri_right.pwm >>= 2;
        }
        else
        {
            motor_dev.dri_right.pwm >>= 2;
        }
        break;

    case DYNAMIC_DIR_LEFT:
        motor_dev.dri_right.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_left.direction = MOTOR_CMD_REVERSE;
        motor_dev.dri_right.pwm = temporary.mem8b[1];

        temporary.mem8b[1] = 0xff - temporary.mem8b[1];
        if (temporary.mem8b[1] == 0)
        {
            temporary.mem8b[1] = 127;
        }
        motor_dev.dri_left.pwm = temporary.mem8b[1];

        break;

    case DYNAMIC_DIR_RIGHT:
        motor_dev.dri_left.direction = MOTOR_CMD_FOREWARD;
        motor_dev.dri_left.pwm = temporary.mem8b[1];

        motor_dev.dri_right.direction = MOTOR_CMD_REVERSE;
        temporary.mem8b[1] = 0xff - temporary.mem8b[1];
        if (temporary.mem8b[1] == 0)
        {
            temporary.mem8b[1] = 127;
        }
        motor_dev.dri_right.pwm = temporary.mem8b[1];
        break;
    }
}

/***************************************************************************
 * function:        Analysis receive data
 * argument in:     none
 * argument out:    none
 * description:     70m
 * *************************************************************************/
void Motor_Analysis_IRrec(void)
{
    if (bat_dev.status & BATTSTA_BIT_CHARGING)
    {
        temporary.mem8b[0] = DYNAMIC_DIR_STOP;
        temporary.mem8b[1] = 0;
    }
    else
    {
        if (dev_infrared.statu & IR_REVSTATU_UPDATA)
        {
            dev_infrared.offline = 0;
            process.gotosleep_tim = 0;

            temporary.mem8b[0] = dev_infrared.dev_rec.data >> 24;
            temporary.mem8b[1] = dev_infrared.dev_rec.data >> 16;
            temporary.mem8b[2] = temporary.mem8b[0] + temporary.mem8b[1];
            if (temporary.mem8b[2] == 0xff)
            {
                temporary.mem8b[0] = dev_infrared.dev_rec.data >> 8;
                temporary.mem8b[1] = dev_infrared.dev_rec.data & 0xff;
                temporary.mem8b[2] = temporary.mem8b[0] + temporary.mem8b[1];
                if (temporary.mem8b[2] == 0xff)
                {
                    //fine - right
                    if (temporary.mem8b[0] & IRKEY_BIT_FINER)//the car is running to left deviation, need to turn right
                    {
                        if (motor_dev.compensation > (COMPENSATION_LEFT_MIN + COMPENSATION_ADJUST_EXTENT))
                        {
                            motor_dev.compensation -= COMPENSATION_ADJUST_EXTENT;
                        }
                        else
                        {
                            motor_dev.compensation = COMPENSATION_LEFT_MIN;
                        }
                    }

                    //fine - left
                    if (temporary.mem8b[0] & IRKEY_BIT_FINEL)
                    {
                        if (motor_dev.compensation < (COMPENSATION_RIGHT_MAX - COMPENSATION_ADJUST_EXTENT))
                        {
                            motor_dev.compensation += COMPENSATION_ADJUST_EXTENT;
                        }
                        else
                        {
                            motor_dev.compensation = COMPENSATION_RIGHT_MAX;
                        }
                    }

                    // forward - press
                    if (temporary.mem8b[0] & IRKEY_BIT_FORWARD)
                    {
                        if (temporary.mem8b[0] & IRKEY_BIT_LEFT)
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_LEFTUP;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                        else if (temporary.mem8b[0] & IRKEY_BIT_RIGHT)
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_RIGHTUP;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                        else
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_FORWARD;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                    }
                    // reverse - press
                    else if (temporary.mem8b[0] & IRKEY_BIT_REVERSE)
                    {
                        if (temporary.mem8b[0] & IRKEY_BIT_LEFT)
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_LEFTDN;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                        else if (temporary.mem8b[0] & IRKEY_BIT_RIGHT)
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_RIGHTDN;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                        else
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_REVERSE;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                    }
                    // only left or right press
                    else if ((temporary.mem8b[0] & IRKEY_BIT_LEFT) || (temporary.mem8b[0] & IRKEY_BIT_RIGHT))
                    {
                        if (temporary.mem8b[0] & IRKEY_BIT_LEFT)
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_LEFT;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                        else
                        {
                            temporary.mem8b[0] = DYNAMIC_DIR_RIGHT;
                            temporary.mem8b[1] = MOTOR_SPEED_CONFIG;
                        }
                    }
                    // all key float
                    else
                    {
                        temporary.mem8b[0] = DYNAMIC_DIR_STOP;
                        temporary.mem8b[1] = 0;
                    }
                }
            }
            dev_infrared.statu &= ~IR_REVSTATU_UPDATA;
        }
        else
        {
            if ((motor_dev.dri_left.pwm > 0) || (motor_dev.dri_right.pwm > 0))
            {
                dev_infrared.offline++;
                if (dev_infrared.offline > 5)
                {
                    dev_infrared.offline = 0;
                    temporary.mem8b[0] = DYNAMIC_DIR_STOP;
                    temporary.mem8b[1] = 0;
                }
            }
        }
    }

    Motor_Algorithm_Running();
}

/***************************************************************************
 * function:        task
 * argument in:     none
 * argument out:    none
 * description:     70ms
 * *************************************************************************/
void Motor_Task_device(void)
{
    Motor_Analysis_IRrec();
    Motor_Operate_Function();
}
