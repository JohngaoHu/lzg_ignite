#ifndef _SHEDULER_H_
#define _SHEDULER_H_

#include "../src/typedef.h"

//------------最大支持8个事件--------------------
//------------为节省RAM，尽量多用宏定义-----------
#define TASKNUM_MAX 4

struct Scheduler_type
{
    u8 ms;
    u8 flag;
    u8 task[TASKNUM_MAX];

    u8 step;
    u8 per20ms;
    u8 per1s;
    u16 gotosleep_tim;
};
extern struct Scheduler_type process;



void Callback_event_task(void);
void Task_Process_Global(void);
#endif
