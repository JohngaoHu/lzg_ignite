#include "includes.h"

/***************************************************************************
 * function:        call back
 * argument in:     none
 * argument out:    none
 * description:     maximum 8 tasks
 * *************************************************************************/
void Callback_event_task(void)
{
    process.ms++;
    if (process.ms < 10)
    {
        return;
    }
    process.ms = 0;

#if TASKNUM_MAX > 0
    if (process.task[0] > 0)
    {
        process.task[0]--;
    }
    else
    {
        process.task[0] = 10;
        process.flag |= 0x01;
    }
#endif

#if TASKNUM_MAX > 1
    if (process.task[1] > 0)
    {
        process.task[1]--;
    }
    else
    {
        process.task[1] = 70;
        process.flag |= 0x02;
    }
#endif

#if TASKNUM_MAX > 2
    if (process.task[2] > 0)
    {
        process.task[2]--;
    }
    else
    {
        process.task[2] = 5;
        process.flag |= 0x04;
    }
#endif

#if TASKNUM_MAX > 3
    if (process.task[3] > 0)
    {
        process.task[3]--;
    }
    else
    {
        process.task[3] = 20;
        process.flag |= 0x08;
    }
#endif

#if TASKNUM_MAX > 4
    if (process.task[4] > 0)
    {
        process.task[4]--;
    }
    else
    {
        process.task[4] = 10;
        process.flag |= 0x10;
    }
#endif

#if TASKNUM_MAX > 5
    if (process.task[5] > 0)
    {
        process.task[5]--;
    }
    else
    {
        process.task[5] = 10;
        process.flag |= 0x20;
    }
#endif

#if TASKNUM_MAX > 6
    if (process.task[6] > 0)
    {
        process.task[6]--;
    }
    else
    {
        process.task[6] = 10;
        process.flag |= 0x40;
    }
#endif

#if TASKNUM_MAX > 7
    if (process.task[7] > 0)
    {
        process.task[7]--;
    }
    else
    {
        process.task[7] = 10;
        process.flag |= 0x80;
    }
#endif
}

/***************************************************************************
 * function:        task process
 * argument in:     none
 * argument out:    none
 * description:
 * *************************************************************************/
void Task_Process_Global(void)
{
    ADC_Convert_Loop(); //不受调度器管理

#if TASKNUM_MAX > 0
    if (process.flag & 0x01)
    {
        process.flag &= ~0x01;
        LED_Task_Display();
    }
#endif

#if TASKNUM_MAX > 1
    if (process.flag & 0x02)
    {
        process.flag &= ~0x02;
        Motor_Task_device();
    }
#endif

#if TASKNUM_MAX > 2
    if (process.flag & 0x04)
    {
        process.flag &= ~0x04;
        Backoffice_Task_Manage();
    }
#endif

#if TASKNUM_MAX > 3
    if (process.flag & 0x08)
    {
        process.flag &= ~0x08;
        Bat_Task_Driver();
    }
#endif

#if TASKNUM_MAX > 4
    if (process.flag & 0x10)
    {
        process.flag &= ~0x10;
    }
#endif

#if TASKNUM_MAX > 5
    if (process.flag & 0x20)
    {
        process.flag &= ~0x20;
    }
#endif

#if TASKNUM_MAX > 6
    if (process.flag & 0x40)
    {
        process.flag &= ~0x40;
    }
#endif

#if TASKNUM_MAX > 7
    if (process.flag & 0x80)
    {
        process.flag &= ~0x80;
    }
#endif
}
